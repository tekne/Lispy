
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#include <parser.h>
#include <lval.h>
#include <types/basic_types.h>
#include <lval_lst.h>
#include <expr.h>
#include <lenv.h>
#include <eval.h>
#include <ops/arithmetic_ops.h>
#include <ops/type_ops.h>
#include <ops/expr_ops.h>
#include <ops/lambda_ops.h>
#include <ops/logical_ops.h>
#include <types/closure.h>
#include <ops/control_ops.h>
#include <ops/file_ops.h>
#include <ops/io_ops.h>

int main(int argc, char** argv)
{

  lenv_t environment = lenv_new_obj();
  lenv_add_arithmetic_ops(&environment);
  lenv_add_expr_ops(&environment);
  lenv_add_lambda_ops(&environment);
  lenv_add_type_ops(&environment);
  lenv_add_logical_ops(&environment);
  lenv_add_control_ops(&environment);
  lenv_add_file_ops(&environment);
  lenv_add_io_ops(&environment);

  lval_t environment_variable = {
    .type = &LVAL_SCOPE_REF,
    .value.Scope = &environment
  };

  lenv_put(&environment, "__env__", environment_variable);

  char debug_mode = 0;

  if(argc > 1)
  {
    for(int i = 1; i < argc; i++)
    {
      lval_t v = load_file(&environment, argv[i]);
      if(v.type == &LVAL_ERR)
      {
        lval_println(v);
        return 1;
      }
    }
    return 0;
  }

  puts("Welcome to Lispy Version 0.0.0.0.1");

  while(!feof(stdin))
  {
    char* input = readline("lispy> ");
    if(!input)
      {
      break;
    }
    if(!strcmp(input, "DEBUG ON"))
    {
      debug_mode = 1;
      continue;
    }
    else if(!strcmp(input, "DEBUG OFF"))
    {
      debug_mode = 0;
      continue;
    }

    add_history(input);

    lval_t x = lval_read(input);
    //lval_print(x);
    //printf(" = ");
    x = lval_eval(&environment, x);
    lval_println(x);
    lval_free(x);

    free(input);
  }

  lenv_free_obj(environment);
  printf("\n");
}
