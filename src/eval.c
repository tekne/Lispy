#include <lval.h>
#include <expr.h>
#include <lval_lst.h>
#include <types/closure.h>

static inline long ampersand_val()
{
  long val = 0;
  char* v = (char*)(&val);
  v[0] = '&';
  return val;
}

lval_t lval_eval(lenv_t* e, lval_t v);

lval_t lval_eval_sexpr(lenv_t* e, lval_t v)
{
  size_t c = lval_lst_count(v.value.List);
  lval_t* arr = lval_lst_arr(v.value.List);
  for(int i = 0; i < c; i++)
  {
    arr[i] = lval_eval(e, arr[i]);
    if(arr[i].type == &LVAL_ERR)
    {
      return lval_take(v, i);
    }
  }

  if(c == 0) {return v;}
  if(c == 1) {return lval_take(v, 0);}

  lval_t f = lval_get(v, 0);
  lval_pop(v, 0);

  lval_t result;
  if(f.type == &LVAL_FUN)
  {
    result = f.value.Function(e, v);
  }
  else if(f.type == &LVAL_CLOS)
  {
    size_t count = lval_lst_count(v.value.List);
    size_t fcount = lval_lst_count(f.value.Closure->formals.value.List);
    size_t fadded = 0;
    lval_t* arr = lval_lst_arr(v.value.List);
    lval_t* farr = lval_lst_arr(f.value.Closure->formals.value.List);
    for(size_t i = 0; i < count; i++)
    {
      if(fadded >= fcount)
      {
        return lval_err("Function passed too many arguments");
      }
      else if((farr[fadded].type == &LVAL_SSYM) &&
      (farr[fadded].value.Int == ampersand_val()))
      {
        break;
      }
      else
      {
        if(farr[fadded].type == &LVAL_SYM)
        {
          lenv_write(
            &f.value.Closure->environment,
            farr[fadded].value.Str,
            arr[i]
          );
        }
        else
        {
          lenv_write_short(
            &f.value.Closure->environment,
            farr[fadded].value.Int,
            arr[i]
          );
        }
        fadded++;
      }
    }
    if(fadded < fcount)
    {
      if((farr[fadded].type == &LVAL_SSYM) &&
      (farr[fadded].value.Int == ampersand_val()))
      {
        if((fadded + 1) < fcount)
        {
          lval_t new_qexpr = lval_qexpr();
          new_qexpr.value.List =
            lval_lst_resize(new_qexpr.value.List, count - fadded);

          lval_t* qarr = lval_lst_arr(new_qexpr.value.List);
          memmove(qarr, arr + fadded, (count - fadded) * sizeof(lval_t));

          fadded++;

          if(farr[fadded].type == &LVAL_SYM)
          {
            lenv_write(
              &f.value.Closure->environment,
              farr[fadded].value.Str,
              new_qexpr
            );
          }
          else
          {
            lenv_write_short(
              &f.value.Closure->environment,
              farr[fadded].value.Int,
              new_qexpr
            );
          }
        }

        fadded++;
      }
    }

    v.value.List = lval_lst_resize(v.value.List, 0);
    lval_free(v);

    if(fadded == fcount)
    {
      f.value.Closure->environment.parent = e;
      result = lval_eval(&f.value.Closure->environment,
        LVAL_SEXPR.constructor(f.value.Closure->body));
    }
    else
    {
      memmove(
        farr, farr + fadded, sizeof(lval_t) * (fcount - fadded));
        f.value.Closure->formals.value.List =
        lval_lst_resize(f.value.Closure->formals.value.List, fcount - fadded
      );
      return f;
    }
  }
  else
  {
    result = expectation_error_type(
      "Callable (Function or Closure)", lval_str(v), v.type->name
    );
    lval_free(v);
  }
  lval_free(f);
  return result;
}

lval_t lval_eval(lenv_t* e, lval_t v)
{
  if((v.type == &LVAL_SYM) || (v.type == &LVAL_SSYM))
  {
    lval_t x = lenv_get_sym(e, v);
    return x.type->constructor(x);
  }
  if(v.type == &LVAL_SEXPR) {return lval_eval_sexpr(e, v);}
  return v;
}
