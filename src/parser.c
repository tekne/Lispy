#include <lval.h>
#include <expr.h>
#include <lval_lst.h>
#include <types/basic_types.h>

#include <ctype.h>
#include <stdlib.h>

lval_t lval_read_expr(char** arr, int is_sexpr);

lval_t lval_read_token(char** arr)
{
  //printf("Reading token\n");
  lval_t v;
  char* string;
  char* num_read;

  if(!arr)
  {
    return lval_err("Null passed to 'read token'!");
  }
  if(!(*arr))
  {
    return lval_err("Read from null!");
  }

  v.value.Int = strtol(*arr, &num_read, 0);
  if(num_read != *arr)
  {
    if(
      (*num_read != '.') && ((**arr == '0') || (*num_read != 'e'))
    )
    {
      v.type = &LVAL_INT;
      *arr = num_read;
      return v;
    }
  }
  v.value.Real = strtod(*arr, &num_read);
  if(num_read != *arr)
  {
    v.type = &LVAL_REAL;
    *arr = num_read;
    return v;
  }

  while(isspace(**arr))
  {
    (*arr)++;
  }
  char* read = *arr;
  if(*read == '(')
  {
    (*arr)++;
    return lval_read_expr(arr, 1);
  }
  else if(*read == '{')
  {
    (*arr)++;
    return lval_read_expr(arr, 0);
  }
  else if((*read == '\0') || (*read == ')') || (*read == '}'))
  {
    v.type = NULL;
    return v;
  }
  else if(*read == '|')
  {
    read++;
    (*arr)++;
    while((*read != '|') && (*read != '\0'))
    {
      read++;
    }
    //printf("At end of quoted symbol, got %c", *read);
  }
  else if(*read == '"')
  {
    read++;
    (*arr)++;
    while((*read != '"') && (*read != '\0'))
    {
      read++;
    }
    //printf("At end of string literal, got %c", *read);
  }
  else
  {
    while(
    !isspace(*read)
    && (*read != '\0')
    && (*read != '}')
    && (*read != ')')
    && (*read != '(')
    && (*read != '{')
    && (*read != '|')
    && (*read != '"')
  )
    {
      read++;
    }
  }

  unsigned diff = read - *arr;

  if(diff <= sizeof(long))
  {
    char* dest = (char*)(&v.value.Int);
    unsigned i = 0;
    while(i < diff)
    {
      dest[i] = (*arr)[i];
      i++;
    }
    while(i < sizeof(long))
    {
      dest[i] = '\0';
      i++;
    }
  }
  else
  {
    string = malloc(diff + 1);
    memcpy(string, *arr, diff);
    string[diff] = '\0';
    v.value.Str = string;
  }

  if(*read == '|')
  {
    read++;
  }
  if(*read == '"')
  {
    if(diff <= sizeof(long))
    {
      v.type = &LVAL_SSTR;
    }
    else
    {
      v.type = &LVAL_STR;
    }
    read++;
  }
  else
  {
    if(diff <= sizeof(long))
    {
      v.type = &LVAL_SSYM;
    }
    else
    {
      v.type = &LVAL_SYM;
    }
  }

  *arr = read;
  return v;
}

lval_t lval_read_expr(char** arr, int is_sexpr)
{
  //printf("\nParsing expression @%d...", is_sexpr);
  lval_t v;
  if(is_sexpr)
  {
    v = lval_sexpr();
  }
  else
  {
    v = lval_qexpr();
  }
  lval_t r = lval_read_token(arr);
  while(r.type != NULL)
  {
    //lval_print(r);
    v.value.List = lval_lst_add(v.value.List, r);
    r = lval_read_token(arr);
  }
  if(!is_sexpr && (**arr != '}'))
  {
    return lval_err("Mismatched '{'!");
  }
  else if((is_sexpr == 1) && (**arr != ')'))
  {
    return lval_err("Mismatched '('!");
  }
  (*arr)++;
  return v;
}

lval_t lval_read(char* arr)
{
  return lval_read_expr(&arr, -1);
}
