#ifndef EXPR_OPS_INCLUDED
#define EXPR_OPS_INCLUDED

#include <lval.h>
#include <lenv.h>
#include <eval.h>
#include <expr.h>
#include <lval_lst.h>

lval_t builtin_head(lenv_t* e, lval_t a)
{
  if(lval_lst_count(a.value.List) != 1)
  {
    lval_free(a);
    return lval_err("Function 'head' passed too many arguments!");
  }
  if(lval_lst_arr(a.value.List)[0].type != &LVAL_QEXPR)
  {
    lval_free(a);
    return lval_err("Function 'head' passed incorrect types!");
  }
  if(!lval_lst_count(lval_lst_arr(a.value.List)[0].value.List))
  {
    lval_free(a);
    return lval_err("Function 'head' passed {}!");
  }


  // Return Qexpr containing only the first element
  lval_t q = lval_take(a, 0);
  lval_t* arr = lval_lst_arr(q.value.List);
  size_t c = lval_lst_count(q.value.List);
  for(int i = 1; i < c; i++)
  {
    lval_free(arr[i]);
  }
  q.value.List = lval_lst_resize(q.value.List, 1);
  return q;
}

lval_t builtin_tail(lenv_t* e, lval_t a)
{
  if(lval_lst_count(a.value.List) != 1)
  {
    lval_free(a);
    return lval_err("Function 'tail' passed too many arguments!");
  }
  if(lval_lst_arr(a.value.List)[0].type != &LVAL_QEXPR)
  {
    lval_free(a);
    return lval_err("Function 'tail' passed incorrect types!");
  }
  if(!lval_lst_count(lval_lst_arr(a.value.List)[0].value.List))
  {
    lval_free(a);
    return lval_err("Function 'tail' passed {}!");
  }

  // Return Qexpr containing only the last element
  lval_t q = lval_take(a, 0);
  lval_t* arr = lval_lst_arr(q.value.List);
  size_t c = lval_lst_count(q.value.List);
  for(int i = 0; i < c - 1; i++)
  {
    lval_free(arr[i]);
  }
  arr[0] = arr[c - 1];
  q.value.List = lval_lst_resize(q.value.List, 1);
  return q;
}

lval_t builtin_list(lenv_t* e, lval_t a)
{
  a.type = &LVAL_QEXPR;
  return a;
}

lval_t builtin_join(lenv_t* e, lval_t a)
{
  lval_t* ptr = lval_lst_arr(a.value.List);
  size_t c = lval_lst_count(a.value.List);
  size_t elems = 0;
  for(size_t i = 0; i < c; i++)
  {
    if(ptr[i].type != &LVAL_QEXPR)
    {
      return lval_err("Function 'join' passed incorrect type.");
    }
    elems += lval_lst_count(ptr[i].value.List);
  }

  lval_t result = lval_qexpr();
  result.value.List = lval_lst_resize(result.value.List, elems);

  lval_t* arr = lval_lst_arr(result.value.List);
  size_t k = 0;
  for(size_t i = 0; i < c; i++)
  {
    size_t sc = lval_lst_count(ptr[i].value.List);
    lval_t* sarr = lval_lst_arr(ptr[i].value.List);
    for(size_t j = 0; j < sc; j++)
    {
      arr[k] = sarr[j];
      k++;
    }
  }

  free(a.value.List);
  return result;
}

lval_t builtin_eval(lenv_t* e, lval_t a)
{
  if(lval_lst_count(a.value.List) != 1)
  {
    lval_free(a);
    return lval_err("Function 'eval' passed too many arguments!");
  }
  if(lval_lst_arr(a.value.List)[0].type != &LVAL_QEXPR)
  {
    lval_free(a);
    return lval_err("Function 'eval' passed incorrect types!");
  }
  lval_t x = lval_take(a, 0);
  x.type = &LVAL_SEXPR;
  return lval_eval(e, x);
}

lval_t builtin_len(lenv_t* e, lval_t a)
{
  if(lval_lst_count(a.value.List) != 1)
  {
    lval_free(a);
    return lval_err("Function 'len' passed too many arguments!");
  }
  if(lval_get(a, 0).type != &LVAL_QEXPR)
  {
    lval_free(a);
    return lval_err("Function 'len' passed incorrect type!");
  }
  lval_t x = lval_int(lval_lst_count(lval_get(a, 0).value.List));
  lval_free(a);
  return x;
}

lval_t builtin_take(lenv_t* e, lval_t a)
{
  if(lval_lst_count(a.value.List) != 2)
  {
    lval_free(a);
    return lval_err("Function 'take' requires exactly 2 arguments");
  }
  if(lval_get(a, 0).type != &LVAL_INT)
  {
    lval_free(a);
    return lval_err("First argument to 'take' must be of type Int");
  }
  if(lval_get(a, 1).type != &LVAL_QEXPR)
  {
    lval_free(a);
    return lval_err("Second argument to 'take' must be of type Qexpr");
  }
  size_t pos = lval_get(a, 0).value.Int;
  if(pos >= lval_lst_count(lval_get(a, 1).value.List))
  {
    lval_free(a);
    return lval_err("Index out of range");
  }
  return lval_take(lval_take(a, 1), pos);
}

lval_t builtin_place(lenv_t* e, lval_t a)
{
  if(lval_lst_count(a.value.List) != 3)
  {
    lval_free(a);
    return lval_err("Function 'put' requires exactly 3 arguments");
  }
  if(lval_get(a, 0).type != &LVAL_INT)
  {
    lval_free(a);
    return lval_err("First argument to 'take' must be of type Int");
  }
  if(lval_get(a, 1).type != &LVAL_QEXPR)
  {
    lval_free(a);
    return lval_err("Second argument to 'take' must be of type Qexpr");
  }
  size_t pos = lval_get(a, 0).value.Int;
  if(pos >= lval_lst_count(lval_get(a, 1).value.List))
  {
    lval_free(a);
    return lval_err("Index out of range");
  }
  lval_t v = lval_get(a, 2);
  lval_lst_arr(a.value.List)[2] = lval_false();
  lval_t l = lval_take(a, 1);
  lval_free(lval_lst_arr(l.value.List)[pos]);
  lval_lst_arr(l.value.List)[pos] = v;
  return l;
}

lenv_t* lenv_add_expr_ops(lenv_t* e)
{
  lenv_add_builtin(e, "join", builtin_join);
  lenv_add_builtin(e, "head", builtin_head);
  lenv_add_builtin(e, "tail", builtin_tail);
  lenv_add_builtin(e, "eval", builtin_eval);
  lenv_add_builtin(e, "len", builtin_len);
  lenv_add_builtin(e, "take", builtin_take);
  lenv_add_builtin(e, "place", builtin_place);
  return e;
}


#endif
