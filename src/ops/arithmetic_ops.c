#include <lval.h>
#include <lenv.h>
#include <lval_lst.h>
#include <types/basic_types.h>

type_t* numeric_type(lval_t* arr, size_t c, int* err, int* hom)
{
  type_t* t = NULL;
  for(int i = 0; i < c; i++)
  {
    if(arr[i].type == &LVAL_REAL)
    {
      if(t != &LVAL_REAL)
      {
        *hom = 0;
      }
      t = &LVAL_REAL;
    }
    else if(arr[i].type == &LVAL_INT)
    {
      if(!t) t = &LVAL_INT;
      if(t != &LVAL_INT) *hom = 0;
    }
    else
    {
      *err = 1;
      return arr[i].type;
    }
  }
  return t;
}

lval_t builtin_add(lenv_t* e, lval_t v)
{
  size_t c = lval_lst_count(v.value.List);
  lval_t* arr = lval_lst_arr(v.value.List);
  int err = 0;
  int hom = 1;

  if(c == 0)
  {
    lval_free(v);
    return lval_err("Cannot have less than 1 argument to '+'");
  }

  type_t* t = numeric_type(arr, c, &err, &hom);
  if(err)
  {
    return expectation_error("Numeric Type (Int, Real)", t->name);
  }

  lval_t result;

  if(t == &LVAL_REAL)
  {
    if(hom)
    {
      result = arr[0];
      for(int i = 1; i < c; i++)
        result.value.Real += arr[i].value.Real;
    }
    else
    {
      result = lval_real_cons(arr[0]);
      for(int i = 1; i < c; i++)
        result.value.Real += lval_real_cons(arr[i]).value.Real;
    }
  }
  else
  {
    result = lval_int_cons(arr[0]);
    for(int i = 1; i < c; i++)
      result.value.Int += arr[i].value.Int;
  }

  lval_free(v);
  return result;
}

lval_t builtin_mul(lenv_t* e, lval_t v)
{
  size_t c = lval_lst_count(v.value.List);
  lval_t* arr = lval_lst_arr(v.value.List);
  int err = 0;
  int hom = 1;

  if(c == 0)
  {
    lval_free(v);
    return lval_err("Cannot have less than 1 argument to '+'");
  }

  type_t* t = numeric_type(arr, c, &err, &hom);
  if(err)
  {
    return expectation_error("Numeric Type (Int, Real)", t->name);
  }

  lval_t result;

  if(t == &LVAL_REAL)
  {
    if(hom)
    {
      result = arr[0];
      for(int i = 1; i < c; i++)
        result.value.Real *= arr[i].value.Real;
    }
    else
    {
      result = lval_real_cons(arr[0]);
      for(int i = 1; i < c; i++)
      {
        result.value.Real *= lval_real_cons(arr[i]).value.Real;
      }
    }
  }
  else
  {
    result = lval_int_cons(arr[0]);
    for(int i = 1; i < c; i++)
    {
      result.value.Int *= arr[i].value.Int;
    }
  }

  lval_free(v);
  return result;
}

lval_t builtin_sub(lenv_t* e, lval_t v)
{
  size_t c = lval_lst_count(v.value.List);
  lval_t* arr = lval_lst_arr(v.value.List);
  int err = 0;
  int hom = 1;

  if(c == 0)
  {
    lval_free(v);
    return lval_err("Cannot have less than 1 argument to '-'");
  }

  type_t* t = numeric_type(arr, c, &err, &hom);
  if(err)
  {
    return expectation_error("Numeric Type (Int, Real)", t->name);
  }

  lval_t result;

  if(t == &LVAL_REAL)
  {
    if(hom)
    {
      result = arr[0];
      for(int i = 1; i < c; i++)
        result.value.Real -= arr[i].value.Real;
    }
    else
    {
      result = lval_real_cons(arr[0]);
      for(int i = 1; i < c; i++)
        result.value.Real -= lval_real_cons(arr[i]).value.Real;
    }
  }
  else
  {
    result = lval_int_cons(arr[0]);
    for(int i = 1; i < c; i++)
      result.value.Int -= arr[i].value.Int;
  }

  if(c == 1)
  {
    if(t == &LVAL_REAL)
    {
      result.value.Real = -result.value.Real;
    }
    else
    {
      result.value.Int = -result.value.Int;
    }
  }

  lval_free(v);
  return result;
}

lval_t builtin_div(lenv_t* e, lval_t v)
{
  size_t c = lval_lst_count(v.value.List);
  lval_t* arr = lval_lst_arr(v.value.List);
  int err = 0;
  int hom = 1;

  if(c == 0)
  {
    lval_free(v);
    return lval_err("Cannot have less than 1 argument to '+'");
  }

  type_t* t = numeric_type(arr, c, &err, &hom);
  if(err)
  {
    return expectation_error("Numeric Type (Int, Real)", t->name);
  }

  lval_t result;

  if(t == &LVAL_REAL)
  {
    if(hom)
    {
      result = arr[0];
      for(int i = 1; i < c; i++)
        result.value.Real /= arr[i].value.Real;
    }
    else
    {
      result = lval_real_cons(arr[0]);
      for(int i = 1; i < c; i++)
      {
        result.value.Real /= lval_real_cons(arr[i]).value.Real;
      }
    }
  }
  else
  {
    result = lval_int_cons(arr[0]);
    for(int i = 1; i < c; i++)
    {
      if(!arr[i].value.Int)
      {
        return lval_err("Division by zero!");
      }
      result.value.Int /= arr[i].value.Int;
    }
  }

  lval_free(v);
  return result;
}

lval_t builtin_lt(lenv_t* e, lval_t a)
{
  size_t c = lval_lst_count(a.value.List);
  lval_t* arr = lval_lst_arr(a.value.List);
  int err = 0;
  int hom = 1;

  if(c < 2)
  {
    lval_free(a);
    return lval_err("Cannot have less than 2 arguments to '<'");
  }

  type_t* t = numeric_type(arr, c, &err, &hom);
  if(err)
  {
    return expectation_error("Numeric Type (Int, Real)", t->name);
  }

  if(t == &LVAL_REAL)
  {
    double comp;
    if(hom)
    {
      comp = arr[0].value.Real;
      for(int i = 1; i < c; i++)
      {
        if(comp >= arr[i].value.Real)
        {
          return lval_false();
        }
        comp = arr[i].value.Real;
      }
    }
    else
    {
      comp = lval_real_cons(arr[0]).value.Real;
      for(int i = 1; i < c; i++)
      {
        if(comp >= lval_real_cons(arr[i]).value.Real)
        {
          return lval_false();
        }
        comp = lval_real_cons(arr[i]).value.Real;
      }
    }
  }
  else
  {
    int comp = arr[0].value.Int;
    for(int i = 1; i < c; i++)
    {
      if(comp >= arr[i].value.Int)
      {
        return lval_false();
      }
      comp = arr[i].value.Int;
    }
  }

  lval_free(a);
  return lval_true();
}

lval_t builtin_gt(lenv_t* e, lval_t a)
{
  size_t c = lval_lst_count(a.value.List);
  lval_t* arr = lval_lst_arr(a.value.List);
  int err = 0;
  int hom = 1;

  if(c < 2)
  {
    lval_free(a);
    return lval_err("Cannot have less than 2 arguments to '>'");
  }

  type_t* t = numeric_type(arr, c, &err, &hom);
  if(err)
  {
    return expectation_error("Numeric Type (Int, Real)", t->name);
  }

  if(t == &LVAL_REAL)
  {
    double comp;
    if(hom)
    {
      comp = arr[0].value.Real;
      for(int i = 1; i < c; i++)
      {
        if(comp <= arr[i].value.Real)
        {
          return lval_false();
        }
        comp = arr[i].value.Real;
      }
    }
    else
    {
      comp = lval_real_cons(arr[0]).value.Real;
      for(int i = 1; i < c; i++)
      {
        if(comp <= lval_real_cons(arr[i]).value.Real)
        {
          return lval_false();
        }
        comp = lval_real_cons(arr[i]).value.Real;
      }
    }
  }
  else
  {
    int comp = arr[0].value.Int;
    for(int i = 1; i < c; i++)
    {
      if(comp <= arr[i].value.Int)
      {
        return lval_false();
      }
      comp = arr[i].value.Int;
    }
  }

  lval_free(a);
  return lval_true();
}

lval_t builtin_leq(lenv_t* e, lval_t a)
{
  size_t c = lval_lst_count(a.value.List);
  lval_t* arr = lval_lst_arr(a.value.List);
  int err = 0;
  int hom = 1;

  if(c < 2)
  {
    lval_free(a);
    return lval_err("Cannot have less than 2 arguments to '<='");
  }

  type_t* t = numeric_type(arr, c, &err, &hom);
  if(err)
  {
    return expectation_error("Numeric Type (Int, Real)", t->name);
  }

  if(t == &LVAL_REAL)
  {
    double comp;
    if(hom)
    {
      comp = arr[0].value.Real;
      for(int i = 1; i < c; i++)
      {
        if(comp > arr[i].value.Real)
        {
          return lval_false();
        }
        comp = arr[i].value.Real;
      }
    }
    else
    {
      comp = lval_real_cons(arr[0]).value.Real;
      for(int i = 1; i < c; i++)
      {
        if(comp > lval_real_cons(arr[i]).value.Real)
        {
          return lval_false();
        }
        comp = lval_real_cons(arr[i]).value.Real;
      }
    }
  }
  else
  {
    int comp = arr[0].value.Int;
    for(int i = 1; i < c; i++)
    {
      if(comp > arr[i].value.Int)
      {
        return lval_false();
      }
      comp = arr[i].value.Int;
    }
  }

  lval_free(a);
  return lval_true();
}

lval_t builtin_geq(lenv_t* e, lval_t a)
{
  size_t c = lval_lst_count(a.value.List);
  lval_t* arr = lval_lst_arr(a.value.List);
  int err = 0;
  int hom = 1;

  if(c < 2)
  {
    lval_free(a);
    return lval_err("Cannot have less than 2 arguments to '>='");
  }

  type_t* t = numeric_type(arr, c, &err, &hom);
  if(err)
  {
    return expectation_error("Numeric Type (Int, Real)", t->name);
  }

  if(t == &LVAL_REAL)
  {
    double comp;
    if(hom)
    {
      comp = arr[0].value.Real;
      for(int i = 1; i < c; i++)
      {
        if(comp < arr[i].value.Real)
        {
          return lval_false();
        }
        comp = arr[i].value.Real;
      }
    }
    else
    {
      comp = lval_real_cons(arr[0]).value.Real;
      for(int i = 1; i < c; i++)
      {
        if(comp < lval_real_cons(arr[i]).value.Real)
        {
          return lval_false();
        }
        comp = lval_real_cons(arr[i]).value.Real;
      }
    }
  }
  else
  {
    int comp = arr[0].value.Int;
    for(int i = 1; i < c; i++)
    {
      if(comp < arr[i].value.Int)
      {
        return lval_false();
      }
      comp = arr[i].value.Int;
    }
  }

  lval_free(a);
  return lval_true();
}

lval_t builtin_eq(lenv_t* e, lval_t a)
{
  size_t c = lval_lst_count(a.value.List);
  lval_t* arr = lval_lst_arr(a.value.List);
  int err = 0;
  int hom = 1;

  if(c < 2)
  {
    lval_free(a);
    return lval_err("Cannot have less than 2 arguments to '=='");
  }

  type_t* t = numeric_type(arr, c, &err, &hom);
  if(err)
  {
    return expectation_error("Numeric Type (Int, Real)", t->name);
  }

  if(t == &LVAL_REAL)
  {
    double comp;
    if(hom)
    {
      comp = arr[0].value.Real;
      for(int i = 1; i < c; i++)
      {
        if(comp != arr[i].value.Real)
        {
          return lval_false();
        }
      }
    }
    else
    {
      comp = lval_real_cons(arr[0]).value.Real;
      for(int i = 1; i < c; i++)
      {
        if(comp != lval_real_cons(arr[i]).value.Real)
        {
          return lval_false();
        }
      }
    }
  }
  else
  {
    int comp = arr[0].value.Int;
    for(int i = 1; i < c; i++)
    {
      if(comp != arr[i].value.Int)
      {
        return lval_false();
      }
    }
  }

  lval_free(a);
  return lval_true();
}

lenv_t* lenv_add_arithmetic_ops(lenv_t* e)
{
  lenv_add_builtin(e, "+", builtin_add);
  lenv_add_builtin(e, "*", builtin_mul);
  lenv_add_builtin(e, "-", builtin_sub);
  lenv_add_builtin(e, "/", builtin_div);
  lenv_add_builtin(e, "<", builtin_lt);
  lenv_add_builtin(e, ">", builtin_gt);
  lenv_add_builtin(e, "<=", builtin_leq);
  lenv_add_builtin(e, ">=", builtin_geq);
  return e;
}
