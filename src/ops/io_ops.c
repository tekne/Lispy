#include <lval.h>
#include <lenv.h>
#include <parser.h>
#include <lval_lst.h>
#include <types/basic_types.h>

lval_t builtin_print(lenv_t* e, lval_t a)
{
  size_t c = lval_lst_count(a.value.List);
  lval_t* arr = lval_lst_arr(a.value.List);
  for(size_t i = 0; i < c; i++)
  {
    if(arr[i].type == &LVAL_STR) arr[i].type = &LVAL_SYM;
    if(arr[i].type == &LVAL_SSTR) arr[i].type = &LVAL_SSYM;
    // Uncomment to disable printing empty sexpr
    /*
    if(arr[i].type == &LVAL_SEXPR)
    {
      if(!lval_lst_count(arr[i].value.List))
      {
        lval_free(arr[i]);
        arr[i].type = &LVAL_SSYM;
        arr[i].value.Int = 0; // Print nothing
      }
    }
    */
    lval_print(arr[i]);
  }
  lval_free(a);
  return lval_sexpr();
}

lval_t builtin_input_str(lenv_t* e, lval_t a)
{
  if(lval_lst_count(a.value.List) != 1)
  {
    lval_free(a);
    return lval_err("'input' requires exactly 1 argument");
  }

  char* str;

  if(lval_get(a, 0).type == &LVAL_STR)
  {
    str = readline(lval_get(a, 0).value.Str);
  }
  else if(lval_get(a, 0).type == &LVAL_SSTR)
  {
    char* prompt = lval_ssym_str(lval_get(a, 0));
    str = readline(prompt);
    free(prompt);
  }
  else
  {
    char* estr = lval_str(lval_get(a, 0));
    lval_t err = expectation_error("String", estr);
    free(estr);
    lval_free(a);
    return err;
  }

  lval_free(a);
  lval_t v = {
    .type = &LVAL_STR,
    .value.Str = str
  };
  return v;
}

lval_t builtin_input(lenv_t* e, lval_t a)
{
  lval_t v = builtin_input_str(e, a);
  if(v.type == &LVAL_ERR)
  {
    return v;
  }
  else if(v.type == &LVAL_STR)
  {
    char* str = v.value.Str;
    if(!str)
    {
      return lval_err("Internal error: 'input_str' returned null pointer!");
    }
    v = lval_read(str);
    free(str);
  }
  else
  {
    v = lval_err("Internal error: did not receive string from 'input_str'");
  }
  return v;
}

lval_t builtin_error(lenv_t* e, lval_t a)
{
  if(lval_lst_count(a.value.List) != 1)
  {
    return
      lval_err("'error' must be passed exactly one argument. How ironic...");
  }
  lval_t v;
  if(lval_get(a, 0).type == &LVAL_STR)
  {
    v = lval_get(a, 0);
    v.type = &LVAL_ERR;
    lval_lst_arr(a.value.List)[0].value.Str = NULL;
  }
  else if(lval_get(a, 0).type == &LVAL_SSTR)
  {
    v.type = &LVAL_ERR;
    v.value.Str = lval_ssym_str(lval_get(a, 0));
  }
  else
  {
    char* estr = lval_str(lval_get(a, 0));
    v = expectation_error("String", estr);
    free(estr);
  }
  lval_free(a);
  return v;
}

lval_t builtin_println(lenv_t* e, lval_t a)
{
  lval_t result = builtin_print(e, a);
  if(result.type != &LVAL_ERR)
  {
    putchar('\n');
  }
  return result;
}

lenv_t* lenv_add_io_ops(lenv_t* e)
{
  lenv_add_builtin(e, "print", builtin_print);
  lenv_add_builtin(e, "println", builtin_println);
  lenv_add_builtin(e, "input_str", builtin_input_str);
  lenv_add_builtin(e, "input", builtin_input);
  lenv_add_builtin(e, "error", builtin_error);
}
