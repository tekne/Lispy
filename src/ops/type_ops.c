#include <lval.h>
#include <lenv.h>
#include <lval_lst.h>

lval_t builtin_typ(lenv_t* e, lval_t a)
{
  if(lval_lst_count(a.value.List) != 1)
  {
    lval_free(a);
    return lval_err("Function 'typ' passed too many arguments!");
  }
  lval_t x = lval_typ_obj(lval_get(a, 0).type);
  lval_free(a);
  return x;
}

lval_t builtin_str(lenv_t* e, lval_t a)
{
  if(lval_lst_count(a.value.List) != 1)
  {
    lval_free(a);
    return lval_err("Function 'typ' passed too many arguments!");
  }
  lval_t x = {
    .type = &LVAL_STR,
    .value = lval_str(lval_get(a, 0))
  };
  lval_free(a);
  return x;
}

lenv_t* lenv_add_type_ops(lenv_t* e)
{
  lenv_add_builtin(e, "typ", builtin_typ);
  lenv_add_builtin(e, "str", builtin_str);
  return e;
}
