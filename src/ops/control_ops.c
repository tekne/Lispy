#include <lval.h>
#include <lenv.h>
#include <eval.h>
#include <lval_lst.h>
#include <ops/logical_ops.h>

lval_t builtin_if(lenv_t* e, lval_t v)
{
  size_t count = lval_lst_count(v.value.List);
  if(count != 3)
  {
    return lval_err("Function 'if' requires exactly 3 arguments");
  }
  lval_t* arr = lval_lst_arr(v.value.List);
  lval_t s = to_logic(arr[0]);
  if(s.type == &LVAL_ERR)
  {
    lval_free(v);
    return s;
  }
  if(arr[1].type != &LVAL_QEXPR)
  {
    char* es = lval_str(arr[1]);
    lval_free(v);
    lval_t err = expectation_error("Q-expression", es);
    free(es);
    return err;
  }
  if(arr[2].type != &LVAL_QEXPR)
  {
    char* es = lval_str(arr[2]);
    lval_free(v);
    lval_t err = expectation_error("Q-expression", es);
    free(es);
    return err;
  }
  arr[1].type = &LVAL_SEXPR;
  arr[2].type = &LVAL_SEXPR;

  if(s.value.Int)
  {
    s = lval_eval(e, arr[1]);
    arr[1] = lval_int(0);
  }
  else
  {
    s = lval_eval(e, arr[2]);
    arr[2] = lval_int(0);
  }
  lval_free(v);
  return s;
}

lval_t builtin_switch(lenv_t* e, lval_t v)
{
  size_t count = lval_lst_count(v.value.List);
  if(count < 2)
  {
    return lval_err("Function 'switch' requires at least 2 arguments");
  }
  lval_t* arr = lval_lst_arr(v.value.List);
  if(arr[0].type != &LVAL_INT)
  {
    char* es = lval_str(arr[0]);
    lval_free(v);
    lval_t err = expectation_error("Int", es);
    free(es);
    return err;
  }

  for(size_t i = 1; i < count; i++)
  {
    if(arr[i].type != &LVAL_QEXPR)
    {
      char* es = lval_str(arr[i]);
      lval_free(v);
      lval_t err = expectation_error("Q-expression", es);
      free(es);
      return err;
    }
  }

  if((arr[0].value.Int >= count - 1) || (arr[0].value.Int < 0))
  {
    lval_free(v);
    return lval_err("Index out of range");
  }
  else
  {
    lval_t result = lval_eval(e, arr[arr[0].value.Int + 1]);
    arr[arr[0].value.Int + 1] = lval_int(0);
    lval_free(v);
    return result;
  }
}

lenv_t* lenv_add_control_ops(lenv_t* e)
{
  lenv_add_builtin(e, "if", builtin_if);
  lenv_add_builtin(e, "switch", builtin_switch);
  return e;
}
