#include <lval.h>
#include <lenv.h>
#include <lval_lst.h>

lval_t to_logic(lval_t v)
{
  if(v.type == &LVAL_REAL)
  {
    return lval_int(v.value.Real != 0);
  }
  else if(v.type == &LVAL_INT)
  {
    return lval_int(v.value.Int != 0);
  }
  else
  {
    char* e = lval_str(v);
    lval_t err = expectation_error("LogicType", e);
    free(e);
    return err;
  }
}

lval_t builtin_log_and(lenv_t* e, lval_t v)
{
  size_t count = lval_lst_count(v.value.List);
  if(!count)
  {
    lval_free(v);
    return lval_err("Function 'and' requires at least one argument");
  }
  lval_t* arr = lval_lst_arr(v.value.List);
  lval_t result = to_logic(arr[0]);
  for(int i = 1; i < count; i++)
  {
    lval_t lres = to_logic(arr[i]);
    if(lres.type != &LVAL_INT)
    {
      lval_free(v);
      return lres;
    }
    result.value.Int = result.value.Int && lres.value.Int;
  }
  lval_free(v);
  return result;
}

lval_t builtin_log_or(lenv_t* e, lval_t v)
{
  size_t count = lval_lst_count(v.value.List);
  if(!count)
  {
    lval_free(v);
    return lval_err("Function 'or' requires at least one argument");
  }
  lval_t* arr = lval_lst_arr(v.value.List);
  lval_t result = to_logic(arr[0]);
  for(int i = 1; i < count; i++)
  {
    lval_t lres = to_logic(arr[i]);
    if(lres.type != &LVAL_INT)
    {
      lval_free(v);
      return lres;
    }
    result.value.Int = result.value.Int || lres.value.Int;
  }
  lval_free(v);
  return result;
}

lval_t builtin_log_xor(lenv_t* e, lval_t v)
{
  size_t count = lval_lst_count(v.value.List);
  if(!count)
  {
    lval_free(v);
    return lval_err("Function 'xor' requires at least one argument");
  }
  lval_t* arr = lval_lst_arr(v.value.List);
  lval_t result = to_logic(arr[0]);
  for(int i = 1; i < count; i++)
  {
    lval_t lres = to_logic(arr[i]);
    if(lres.type != &LVAL_INT)
    {
      lval_free(v);
      return lres;
    }
    result.value.Int = (result.value.Int != lres.value.Int);
  }
  lval_free(v);
  return result;
}

lval_t builtin_log_not(lenv_t* e, lval_t v)
{
  size_t count = lval_lst_count(v.value.List);
  if(count != 1)
  {
    lval_free(v);
    return lval_err("Function 'not' requires exactly one argument");
  }
  lval_t result = to_logic(lval_get(v, 0));
  result.value.Int = !result.value.Int;
  lval_free(v);
  return result;
}

lenv_t* lenv_add_logical_ops(lenv_t* e)
{
  lenv_add_builtin(e, "and", builtin_log_and);
  lenv_add_builtin(e, "or", builtin_log_or);
  lenv_add_builtin(e, "xor", builtin_log_xor);
  lenv_add_builtin(e, "not", builtin_log_not);
  return e;
}
