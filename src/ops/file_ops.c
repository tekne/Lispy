#include <stdio.h>

#include <lval.h>
#include <expr.h>
#include <eval.h>
#include <lval_lst.h>
#include <parser.h>

char* read_file_line(FILE* f)
{
  if(!f) return NULL;

  int r = 0;
  r = getc(f);
  ungetc(r, f);
  if(r == EOF)
  {
    return NULL;
  }

  fpos_t *p = malloc(sizeof(fpos_t)), *el = malloc(sizeof(fpos_t));
  if((!p) || (!el) || (fgetpos(f, p)))
  {
    free(p);
    free(el);
    return NULL;
  }
  int semicolon = 0;
  size_t l = 0;

  while(1)
  {
    r = getc(f);
    if((r == '\0') || (r == '\n') || (r == '\r') || (r == EOF))
    {
      if(fgetpos(f, el))
      {
        free(p);
        free(el);
        return NULL;
      }
      break;
    }
    if(r == ';') semicolon = 1;
    if(!semicolon) l++;
  }

  if(fsetpos(f, p))
  {
    free(p);
    free(el);
    return NULL;
  }

  char* result = malloc(l + 1);
  if((!result) || (!fgets(result, l + 1, f)))
  {
    free(result);
    free(p);
    free(el);
    return NULL;
  }

  if(fsetpos(f, el))
  {
    free(result);
    free(p);
    free(el);
    return NULL;
  }

  free(p);
  free(el);
  return result;
}

lval_t load_file(lenv_t* e, char* str)
{
  lval_t v;

  FILE* f = fopen(str, "r");
  if(!f)
  {
    return lval_err("Cannot open file");
  }

  char* line = read_file_line(f);
  while(line)
  {
    v = lval_eval(e, lval_read(line));
    free(line);

    if(v.type == &LVAL_ERR)
    {
      fclose(f);
      return v;
    }
    else
    {
      lval_free(v);
      line = read_file_line(f);
    }
  }

  fclose(f);
  return lval_sexpr();
}

lval_t builtin_load(lenv_t* e, lval_t a)
{
  if(lval_lst_count(a.value.List) != 1)
  {
    return lval_err("'load' must have exactly one argument");
  }
  lval_t v = lval_get(a, 0);
  if((v.type != &LVAL_STR) && (v.type != &LVAL_SSTR))
  {
    return lval_err("Argument to 'load' must be of type String");
  }

  int sstr = 0;
  char* str;
  if(v.type == &LVAL_SSTR)
  {
    sstr = 1;
    str = lval_ssym_str(v);
  }
  else
  {
    str = v.value.Str;
    v.value.Str = NULL;
  }
  if(!str)
  {
    return lval_err("String is null pointer");
  }

  lval_t result = load_file(e, str);

  lval_free(a);
  free(str);

  return result;
}

lenv_t* lenv_add_file_ops(lenv_t* e)
{
  lenv_add_builtin(e, "load", builtin_load);
  return e;
}
