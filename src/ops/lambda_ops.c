#include <lval.h>
#include <lenv.h>
#include <lval_lst.h>
#include <types/closure.h>

static inline long ampersand_val()
{
  long val = 0;
  char* v = (char*)(&val);
  v[0] = '&';
  return val;
}

lval_t builtin_lambda(lenv_t* e, lval_t a)
{
  if(lval_lst_count(a.value.List) != 2)
  {
    lval_free(a);
    return lval_err("\\ must have exactly two arguments!");
  }
  lval_t* arr = lval_lst_arr(a.value.List);
  if(arr[0].type != &LVAL_QEXPR)
  {
    lval_free(a);
    return lval_err("First argument to \\ must be a Q-expr");
  }
  if(arr[1].type != &LVAL_QEXPR)
  {
    lval_free(a);
    return lval_err("Second argument to \\ must be a Q-expr");
  }
  lval_t* symbols = lval_lst_arr(arr[0].value.List);
  size_t count = lval_lst_count(arr[0].value.List);
  int varargs = 0;
  for(int i = 0; i < count; i++)
  {
    if((symbols[i].type != &LVAL_SYM) && (symbols[i].type != &LVAL_SSYM))
    {
      char* str = symbols[i].type->str(symbols[i]);
      lval_t e = expectation_error("Symbol", str);
      free(str);
      return e;
    }
    else if(varargs == 1)
    {
      if(i + 1 != count)
      {
        return lval_err("Function format invalid: \
        symbol '&' followed by more than one symbol");
      }
      else
      {
        varargs = 2;
      }
    }
    else if((symbols[i].type == &LVAL_SSYM)
    && (symbols[i].value.Int == ampersand_val()))
    {
      varargs = 1;
    }
  }

  lval_t lambda = lval_closure(lenv_new_obj(), arr[0], arr[1]);
  free(a.value.List);
  return lambda;
}

lval_t builtin_var(lenv_t* e, lval_t a, int global)
{
  lval_t* arr = lval_lst_arr(a.value.List);
  if(!lval_lst_count(a.value.List))
  {
    return lval_err("Variable definition must have an argument!");
  }
  if(arr[0].type != &LVAL_QEXPR)
  {
    return lval_err("First argument to variable definition must be a Q-expression");
  }
  lval_t* syms = lval_lst_arr(arr[0].value.List);
  size_t count = lval_lst_count(arr[0].value.List);

  for(size_t i = 0; i < count; i++)
  {
    if((syms[i].type != &LVAL_SYM) && (syms[i].type != &LVAL_SSYM))
    {
      return lval_err("Cannot define non-symbol!");
    }
  }

  if(count != lval_lst_count(a.value.List) - 1)
  {
    return lval_err("Too few symbols provided to bind arguments!");
  }

  for(int i = 0; i < count; i++)
  {
    if(global)
    {
      lenv_put_sym_global(e, syms[i], arr[i + 1]);
    }
    else
    {
      lenv_put_sym(e, syms[i], arr[i + 1]);
    }
  }

  lval_free(a);
  return lval_sexpr();
}

lval_t builtin_def(lenv_t* e, lval_t a)
{
  return builtin_var(e, a, 1);
}

lval_t builtin_put(lenv_t* e, lval_t a)
{
  return builtin_var(e, a, 0);
}

lenv_t* lenv_add_lambda_ops(lenv_t* e)
{
  lenv_add_builtin(e, "\\", builtin_lambda);
  lenv_add_builtin(e, "def", builtin_def);
  lenv_add_builtin(e, "=", builtin_put);
  return e;
}
