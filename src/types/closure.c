#include <lval.h>
#include <lenv.h>
#include <lval_lst.h>
#include <types/basic_types.h>
#include <expr.h>

struct closure_t
{
  lenv_t environment;
  lval_t formals;
  lval_t body;
};

lval_t lval_closure_cons(lval_t v);

void lval_closure_dest(lval_t v)
{
  lenv_free_obj(v.value.Closure->environment);
  lval_free(v.value.Closure->formals);
  lval_free(v.value.Closure->body);
  free(v.value.Closure);
}

char* lval_closure_str(lval_t v)
{
  char* formals = v.value.Closure->formals.type->str(v.value.Closure->formals);
  char* body = v.value.Closure->body.type->str(v.value.Closure->body);
  char* result = NULL;
  if(formals && body)
  {
    result = malloc(1 +
      strlen("(\\ ") + strlen(formals) +
      strlen(" ") + strlen(body) + strlen(")")
    );
    sprintf(result, "(\\ %s %s)", formals, body);
  }
  else if(body)
  {
    result = malloc(1 + strlen("ERROR: corrupted formals"));
    strcpy(result, "ERROR: corrupted formals");
  }
  else if(formals)
  {
    result = malloc(1 + strlen("ERROR: corrupted body"));
    strcpy(result, "ERROR: corrupted body");
  }
  else
  {
    result = malloc(1 + strlen("ERROR: corrupted body and formals"));
    strcpy(result, "ERROR: corrupted body and formals");
  }
  free(formals);
  free(body);
  return result;
}

type_t LVAL_CLOS =
{
  .name = "Closure",
  .constructor = lval_closure_cons,
  .destructor = lval_closure_dest,
  .str = lval_closure_str,
  .free_string = 0
};

lval_t lval_closure(lenv_t environment, lval_t formals, lval_t body)
{
  lval_t r;
  r.type = &LVAL_CLOS;
  r.value.Closure = malloc(sizeof(closure_t));
  r.value.Closure->environment = environment;
  r.value.Closure->formals = formals;
  r.value.Closure->body = body;
  return r;
}

lval_t lval_closure_cons(lval_t v)
{
  lval_t r;
  r.type = &LVAL_CLOS;
  if(v.type == &LVAL_CLOS)
  {
    //printf("Constructing closure...\n");
    return lval_closure(
      lenv_copy_obj(v.value.Closure->environment),
      v.value.Closure->formals.type->constructor(v.value.Closure->formals),
      v.value.Closure->body.type->constructor(v.value.Closure->body)
    );
  }
  else
  {
    return constructor_error(LVAL_CLOS, v);
  }
}
