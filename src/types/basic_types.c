#include <string.h>
#include <stdlib.h>

#include <lval.h>

#define REAL_PRECISION 7
#define MAX_EXPONENT_LENGTH 5

// C-value constructors

lval_t lval_int(const long);
lval_t lval_true();
lval_t lval_false();
lval_t lval_real(double);
lval_t lval_sym(const char*);
lval_t lval_ssym(long);
lval_t lval_ssym_d(const char*);
lval_t lval_symbol(const char*);
lval_t lval_str_d(const char*);
lval_t lval_sstr(long);
lval_t lval_sstr_d(const char*);
lval_t lval_string(const char*);
lval_t lval_fun(lfun_t);

// Lisp-value constructors

lval_t lval_int_cons(lval_t);
lval_t lval_real_cons(lval_t);
lval_t lval_sym_cons(lval_t);
lval_t lval_ssym_cons(lval_t);
lval_t lval_str_cons(lval_t);
lval_t lval_sstr_cons(lval_t);
lval_t lval_fun_cons(lval_t);

// String functions

char* lval_int_str(lval_t);
char* lval_real_str(lval_t);
char* lval_ssym_str(lval_t);
char* lval_str_str(lval_t);
char* lval_sstr_str(lval_t);

// Short string utility

long str_to_sstr(const char* s)
{
  size_t c = strlen(s);
  if(c <= sizeof(long))
  {
    unsigned i = 0;
    long result;
    char* rptr = (char*)&result;
    while(i < c)
    {
      rptr[i] = s[i];
      i++;
    }
    while(i < sizeof(long))
    {
      rptr[i] = '\0';
      i++;
    }
    return result;
  }
  else
  {
    return 0;
  }
}

// Types

type_t LVAL_INT = {
  .name = "Int",
  .constructor = lval_int_cons,
  .destructor = NULL,
  .str = lval_int_str,
  .free_string = 0
};

type_t LVAL_REAL = {
  .name = "Real",
  .constructor = lval_real_cons,
  .destructor = NULL,
  .str = lval_real_str,
  .free_string = 0
};

type_t LVAL_SYM = {
  .name = "Symbol",
  .constructor = lval_sym_cons,
  .destructor = free_destructor,
  .str = identity_str,
  .free_string = 0
};

type_t LVAL_SSYM = {
  .name = "Symbol (Short)",
  .constructor = lval_ssym_cons,
  .destructor = NULL,
  .str = lval_ssym_str,
  .free_string = 0
};

type_t LVAL_STR = {
  .name = "String",
  .constructor = lval_str_cons,
  .destructor = free_destructor,
  .str = lval_str_str,
  .free_string = 0
};

type_t LVAL_SSTR = {
  .name = "String (Short)",
  .constructor = lval_sstr_cons,
  .destructor = NULL,
  .str = lval_sstr_str,
  .free_string = 0
};

type_t LVAL_FUN = {
  .name = "Function",
  .constructor = lval_fun_cons,
  .destructor = NULL,
  .str = NULL,
  .free_string = 0
};

lval_t lval_int(const long x)
{
  lval_t v;
  v.type = &LVAL_INT;
  v.value.Int = x;
  return v;
}

lval_t lval_true()
{
  lval_t v = {
    .type = &LVAL_INT,
    .value.Int = 1
  };
  return v;
}

lval_t lval_false()
{
  lval_t v = {
    .type = &LVAL_INT,
    .value.Int = 0
  };
  return v;
}

lval_t lval_real(const double x)
{
  lval_t v;
  v.type = &LVAL_REAL;
  v.value.Real = x;
  return v;
}

lval_t lval_sym(const char* x)
{
  lval_t v;
  v.type = &LVAL_SYM;
  v.value.Str = malloc(strlen(x) + 1);
  strcpy(v.value.Str, x);
  return v;
}

lval_t lval_ssym(long x)
{
  lval_t v;
  v.type = &LVAL_SSYM;
  v.value.Int = x;
  return v;
}

lval_t lval_ssym_d(const char* x)
{
  lval_t v;
  v.type = &LVAL_SSYM;
  size_t c = strlen(x);
  if(c > sizeof(long)) return lval_err("Input string too large!");
  for(unsigned i = 0; i < c; i++)
  {
    v.value.ShortStr[i] = x[i];
  }
  for(size_t i = c; i < sizeof(long); i++)
  {
    v.value.ShortStr[i] = '\0';
    // Note that short strings do not necessarily need a terminating null
  }
  return v;
}

lval_t lval_symbol(const char* x)
{
  size_t c = strlen(x);
  if(c <= sizeof(long))
  {
    return lval_ssym_d(x);
  }
  else
  {
    return lval_sym(x);
  }
}

lval_t lval_str_d(const char* x)
{
  lval_t v = lval_sym(x);
  v.type = &LVAL_STR;
  return v;
}

lval_t lval_sstr(long x)
{
  lval_t v = lval_ssym(x);
  v.type = &LVAL_SSTR;
  return v;
}

lval_t lval_sstr_d(const char* x)
{
  lval_t v = lval_ssym_d(x);
  if(v.type != &LVAL_ERR) v.type = &LVAL_STR;
  return v;
}

lval_t lval_string(const char* x)
{
  lval_t v = lval_symbol(x);
  v.type = &LVAL_STR;
  return v;
}

lval_t lval_fun(lfun_t func)
{
  lval_t v;
  v.type = &LVAL_FUN;
  v.value.Function = func;
  return v;
}

lval_t lval_int_cons(lval_t v)
{
  if(v.type == &LVAL_INT)
  {
    return v;
  }
  else
  {
    return constructor_error(LVAL_INT, v);
  }
}

lval_t lval_real_cons(lval_t v)
{
  if(v.type == &LVAL_REAL)
  {
    return v;
  }
  else if(v.type == &LVAL_INT)
  {
    return lval_real(v.value.Int);
  }
  else
  {
    return constructor_error(LVAL_REAL, v);
  }
}

lval_t lval_sym_cons(lval_t v)
{
  if(v.type == &LVAL_SYM)
  {
    return lval_sym(v.value.Str);
  }
  else if(v.type == &LVAL_SSYM)
  {
    char* src = lval_ssym_str(v);
    lval_t result = lval_sym(src);
    free(src);
    return result;
  }
  else
  {
    return constructor_error(LVAL_SYM, v);
  }
}

lval_t lval_ssym_cons(lval_t v)
{
  if(v.type == &LVAL_SSYM)
  {
    return v;
  }
  else if(v.type == &LVAL_SYM)
  {
    return lval_ssym_d(v.value.Str);
  }
  else
  {
    return constructor_error(LVAL_SSYM, v);
  }
}

lval_t lval_symbol_cons(lval_t v)
{
  if(v.type == &LVAL_SSYM)
  {
    return v;
  }
  else if(v.type == &LVAL_SYM)
  {
    return lval_symbol(v.value.Str);
  }
  else
  {
    return constructor_error(LVAL_SYM, v);
  }
}

lval_t lval_str_cons(lval_t v)
{
  if(v.type == &LVAL_STR)
  {
    return lval_str_d(v.value.Str);
  }
  else if(v.type == &LVAL_SSTR)
  {
    char* src = lval_ssym_str(v);
    lval_t result = lval_str_d(src);
    free(src);
    return result;
  }
  else
  {
    return constructor_error(LVAL_STR, v);
  }
}

inline lval_t lval_sstr_cons(lval_t v)
{
  if(v.type == &LVAL_SSTR)
  {
    return v;
  }
  else if(v.type == &LVAL_STR)
  {
    return lval_sstr_d(v.value.Str);
  }
  else
  {
    return constructor_error(LVAL_STR, v);
  }
}

lval_t lval_fun_cons(lval_t v)
{
  if(v.type == &LVAL_FUN)
  {
    return v;
  }
  else
  {
    return constructor_error(LVAL_FUN, v);
  }
}

char* lval_int_str(lval_t v)
{
  char* result = malloc(sizeof(long) * 3);
  sprintf(result, "%ld", v.value.Int);
  return result;
}

char* lval_real_str(lval_t v)
{
  char* result = malloc(REAL_PRECISION + MAX_EXPONENT_LENGTH + 3);
  sprintf(result, "%.7g", v.value.Real);
  return result;
}

char* lval_ssym_str(lval_t v)
{
  char* result = malloc(sizeof(long) + 1);
  for(unsigned i = 0; i < sizeof(long); i++)
  {
    result[i] = v.value.ShortStr[i];
  }
  result[sizeof(long)] = '\0';
  return result;
}

char* lval_str_str(lval_t v)
{
  size_t c = strlen(v.value.Str);
  char* result = malloc(c + 3);
  result[0] = '"';
  strcpy(result + 1, v.value.Str);
  result[c + 1] = '"';
  result[c + 2] = '\0';
  return result;
}

char* lval_sstr_str(lval_t v)
{
  char* result = malloc(sizeof(long) + 3);
  result[0] = '"';
  unsigned i = 0;
  while((i < sizeof(long)) && (v.value.ShortStr[i]))
  {
    result[i + 1] = v.value.ShortStr[i];
    i++;
  }
  result[i + 1] = '"';
  result[i + 2] = '\0';
  return result;
}
