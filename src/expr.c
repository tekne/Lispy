#include "lval.h"
#include "lval_lst.h"

lval_t lval_sexpr_cons(lval_t v);
void lval_lst_dest(lval_t v);
char* lval_sexpr_str(lval_t v);

type_t LVAL_SEXPR = {
  .name = "Sexpr",
  .constructor = lval_sexpr_cons,
  .destructor = lval_lst_dest,
  .str = lval_sexpr_str,
  .free_string = 0
};

lval_t lval_qexpr_cons(lval_t v);
char* lval_qexpr_str(lval_t v);

type_t LVAL_QEXPR = {
  .name = "Qexpr",
  .constructor = lval_qexpr_cons,
  .destructor = lval_lst_dest,
  .str = lval_qexpr_str,
  .free_string = 0
};

 lval_t lval_sexpr_cons(lval_t v)
{
  if(v.type == &LVAL_SEXPR || v.type == &LVAL_QEXPR)
  {
    v.type = &LVAL_SEXPR;
    v.value.List = lval_lst_cpy(v.value.List);
    return v;
  }
  else
  {
    return constructor_error(LVAL_SEXPR, v);
  }
}

 lval_t lval_qexpr_cons(lval_t v)
{
  if(v.type == &LVAL_SEXPR || v.type == &LVAL_QEXPR)
  {
    v.type = &LVAL_QEXPR;
    v.value.List = lval_lst_cpy(v.value.List);
    return v;
  }
  else
  {
    return constructor_error(LVAL_QEXPR, v);
  }
}

 void lval_lst_dest(lval_t v)
{
  lval_lst_free(v.value.List);
}

 char* lval_sexpr_str(lval_t v)
{
  return lval_lst_str(v.value.List, '(', ')');
}

 char* lval_qexpr_str(lval_t v)
{
  return lval_lst_str(v.value.List, '{', '}');
}

lval_t lval_sexpr()
{
  lval_t v;
  v.type = &LVAL_SEXPR;
  v.value.List = lval_lst_new();
  return v;
}

lval_t lval_qexpr()
{
  lval_t v;
  v.type = &LVAL_QEXPR;
  v.value.List = lval_lst_new();
  return v;
}


lval_t lval_add(lval_t v, lval_t x)
{
  if((v.type != &LVAL_SEXPR) && (v.type != &LVAL_QEXPR))
  {
    return lval_err("Value not S or Q expr!");
  }
  else
  {
    v.value.List = lval_lst_add(v.value.List, x);
    return v;
  }
}
