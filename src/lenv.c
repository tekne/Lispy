#include <lval.h>
#include <types/basic_types.h>

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

typedef struct lenv_t
{
  size_t count;
  size_t short_count;
  char** symbols;
  long* short_symbols;
  lval_t* values;
  lval_t* short_values;
  lenv_t* parent;
} lenv_t;

lenv_t lenv_new_obj()
{
  lenv_t e;
  e.count = 0;
  e.short_count = 0;
  e.symbols = NULL;
  e.values = NULL;
  e.short_values = NULL;
  e.short_symbols = NULL;
  e.parent = NULL;
  return e;
}

lenv_t lenv_copy_obj(lenv_t e)
{
  lenv_t r;
  r.count = e.count;
  r.short_count = e.short_count;
  r.symbols = (char**)malloc(sizeof(char*) * r.count);
  r.short_symbols = (long*)malloc(sizeof(char*) * r.short_count);
  r.values = (lval_t*)malloc(sizeof(lval_t) * r.count);
  r.short_values = (lval_t*)malloc(sizeof(lval_t) * r.short_count);
  for(size_t i = 0; i < r.count; i++)
  {
    r.values[i] = e.values[i].type->constructor(e.values[i]);
    r.symbols[i] = malloc(strlen(e.symbols[i]));
    strcpy(r.symbols[i], e.symbols[i]);
  }
  for(size_t i = 0; i < r.short_count; i++)
  {
    r.short_values[i] = e.short_values[i].type->constructor(e.short_values[i]);
    r.short_symbols[i] = e.short_symbols[i];
  }
  r.parent = e.parent;
  return r;
}

lenv_t* lenv_new()
{
  lenv_t* e = malloc(sizeof(lenv_t));
  *e = lenv_new_obj();
  return e;
}

void lenv_dump_contents(lenv_t* e)
{
  free(e->symbols);
  free(e->values);
  e->symbols = NULL;
  e->short_symbols = NULL;
  e->values = NULL;
  e->short_values = NULL;
  e->count = 0;
  e->short_count = 0;
}

void lenv_free_obj(lenv_t e)
{
  //printf("Freeing object with %zu long symbols and %zu short symbols\n", e.count, e.short_count);
  for(int i = 0; i < e.count; i++)
  {
    free(e.symbols[i]);
    lval_free(e.values[i]);
  }
  for(int i = 0; i < e.count; i++)
  {
    lval_free(e.values[i]);
  }
  for(int i = 0; i < e.short_count; i++)
  {
    lval_free(e.short_values[i]);
  }
  free(e.symbols);
  free(e.short_symbols);
  free(e.values);
  free(e.short_values);
}


void lenv_free(lenv_t* e)
{
  lenv_free_obj(*e);
  free(e);
}

lval_t* lenv_get_short_ptr(lenv_t* e, long k)
{
  if(!e) return NULL;
  lval_t* short_vals = e->short_values;
  long* symbols = e->short_symbols;
  ptrdiff_t r = (ptrdiff_t)e->short_count - 1;
  ptrdiff_t l = 0;
  ptrdiff_t m;
  while(l <= r)
  {
    m = (l + r)/2;
    if(k == symbols[m])
    {
      return m + short_vals;
    }
    else if(k > symbols[m])
    {
      r = m - 1;
    }
    else if(k < symbols[m])
    {
      l = m + 1;
    }
  }
  return lenv_get_short_ptr(e->parent, k);
}

lval_t* lenv_get_long_ptr(lenv_t* e, char* k)
{
  if(!e) return NULL;
  ptrdiff_t r = (ptrdiff_t)e->count - 1;
  ptrdiff_t l = 0;
  ptrdiff_t m;
  int diff;
  while(l <= r)
  {
    m = (l + r)/2;
    diff = strcmp(k, e->symbols[m]);
    if(!diff)
    {
      return m + e->values;
    }
    else if(diff > 0)
    {
      r = m - 1;
    }
    else
    {
      l = m + 1;
    }
  }
  return lenv_get_long_ptr(e->parent, k);
}

lval_t* lenv_get_ptr(lenv_t* e, char* k)
{
  long lk = str_to_sstr(k);
  if(lk)
  {
    return lenv_get_short_ptr(e, lk);
  }
  else
  {
    return lenv_get_long_ptr(e, k);
  }
}

lval_t lenv_get(lenv_t* e, char* k)
{
  lval_t* ptr = lenv_get_ptr(e, k);
  if(ptr)
  {
    return *ptr;
  }
  else
  {
    char* err = malloc(strlen(k) + strlen("Unbound symbol ") + 1);
    sprintf(err, "Unbound symbol %s", k);
    lval_t result = lval_err(err);
    free(err);
    return result;
  }
}

lval_t lenv_get_sym(lenv_t* e, lval_t v)
{
  if(v.type == &LVAL_SYM)
  {
    lval_t* ptr = lenv_get_ptr(e, v.value.Str);
    if(ptr) return *ptr;
  }
  else if(v.type == &LVAL_SSYM)
  {
    lval_t* ptr = lenv_get_short_ptr(e, v.value.Int);
    if(ptr) return *ptr;
  }
  return v;
}

ptrdiff_t lenv_long_pos(lenv_t* e, char* k, int* eq)
{
  if(!e)
  {
    return -1;
  }
  ptrdiff_t r = (ptrdiff_t)e->count - 1;
  ptrdiff_t l = 0;
  ptrdiff_t m;
  int diff;
  while(l <= r)
  {
    m = (l + r)/2;
    diff = strcmp(k, e->symbols[m]);
    if(!diff)
    {
      return m;
    }
    else if(diff > 0)
    {
      r = m - 1;
    }
    else
    {
      l = m + 1;
    }
  }
  *eq = 0;
  return l;
}

ptrdiff_t lenv_short_pos(lenv_t* e, long k, int* eq)
{
  if(!e)
  {
    return -1;
  }
  long* symbols = e->short_symbols;
  ptrdiff_t r = (ptrdiff_t)e->short_count - 1;
  ptrdiff_t l = 0;
  ptrdiff_t m;
  while(l <= r)
  {
    m = (l + r)/2;
    if(k == symbols[m])
    {
      return m;
    }
    else if(k > symbols[m])
    {
      r = m - 1;
    }
    else
    {
      l = m + 1;
    }
  }
  *eq = 0;
  return l;
}

lenv_t* lenv_insert_long(lenv_t* e, size_t pos, char* k, lval_t v)
{
  e->symbols = realloc(e->symbols, (e->count + 1) * sizeof(char*));
  e->values = realloc(e->values, (e->count + 1) * sizeof(lval_t));

  memmove(
    e->symbols + pos + 1,
    e->symbols + pos,
    (size_t)(e->count - pos) * sizeof(char*)
  );

  memmove(
    e->values + pos + 1,
    e->values + pos,
    (size_t)(e->count - pos) * sizeof(lval_t)
  );

  e->symbols[pos] = malloc(strlen(k) + 1);
  strcpy(e->symbols[pos], k);
  e->values[pos] = v;

  e->count++;

  return e;
}

lenv_t* lenv_insert_short(lenv_t* e, size_t pos, long k, lval_t v)
{
  e->short_symbols = realloc(e->short_symbols, (e->short_count + 1) * sizeof(char*));
  e->short_values = realloc(e->short_values, (e->short_count + 1) * sizeof(lval_t));

  memmove(
    e->short_symbols + pos + 1,
    e->short_symbols + pos,
    (size_t)(e->short_count - pos) * sizeof(long)
  );

  memmove(
    e->short_values + pos + 1,
    e->short_values + pos,
    (size_t)(e->short_count - pos) * sizeof(lval_t)
  );

  e->short_symbols[pos] = k;
  e->short_values[pos] = v;
  e->short_count++;

  return e;
}

lenv_t* lenv_write_long(lenv_t* e, char* k, lval_t v)
{
  int eq = 1;
  ptrdiff_t pos = lenv_long_pos(e, k, &eq);
  if(eq)
  {
    lval_free(e->values[pos]);
    e->values[pos] = v;
  }
  else
  {
    lenv_insert_long(e, pos, k, v);
  }
  return e;
}

lenv_t* lenv_write_short(lenv_t* e, long k, lval_t v)
{
  int eq = 1;
  ptrdiff_t pos = lenv_short_pos(e, k, &eq);
  if(eq)
  {
    lval_free(e->short_values[pos]);
    e->short_values[pos] = v;
  }
  else
  {
    lenv_insert_short(e, pos, k, v);
  }
  return e;
}

lenv_t* lenv_write(lenv_t* e, char* k, lval_t v)
{
  long lk = str_to_sstr(k);
  if(lk)
  {
    return lenv_write_short(e, lk, v);
  }
  else
  {
    return lenv_write_long(e, k, v);
  }
}

lenv_t* lenv_put_long(lenv_t* e, char* k, lval_t v)
{
  return lenv_write_long(e, k, v.type->constructor(v));
}
lenv_t* lenv_put_short(lenv_t* e, long k, lval_t v)
{
  return lenv_write_short(e, k, v.type->constructor(v));
}
lenv_t* lenv_put(lenv_t* e, char* k, lval_t v)
{
  return lenv_write(e, k, v.type->constructor(v));
}

lenv_t* lenv_put_sym(lenv_t* e, lval_t k, lval_t v)
{
  lval_t n = v.type->constructor(v);
  if(k.type == &LVAL_SYM)
  {
    return lenv_write(e, k.value.Str, n);
  }
  else if(k.type == &LVAL_SSYM)
  {
    return lenv_write_short(e, k.value.Int, n);
  }
  return e;
}

void lenv_write_global_short(lenv_t* e, long k, lval_t v)
{
  if(e)
  {
    if(e->parent)
    {
      lenv_write_global_short(e->parent, k, v);
    }
    else
    {
      lenv_write_short(e, k, v);
    }
  }
}

void lenv_write_global_long(lenv_t* e, char* k, lval_t v)
{
  if(e)
  {
    if(e->parent)
    {
      lenv_write_global_long(e->parent, k, v);
    }
    else
    {
      lenv_write_long(e, k, v);
    }
  }
}

void lenv_write_global(lenv_t* e, char* k, lval_t v)
{
  if(e)
  {
    if(e->parent)
    {
      lenv_write_global(e->parent, k, v);
    }
    else
    {
      lenv_write(e, k, v);
    }
  }
}

void lenv_put_global(lenv_t* e, char* k, lval_t v)
{
  lenv_write_global(e, k, v.type->constructor(v));
}

void lenv_put_sym_global(lenv_t* e, lval_t k, lval_t v)
{
  if(k.type == &LVAL_SYM)
  {
    lenv_put_global(e, k.value.Str, v);
  }
  else if(k.type == &LVAL_SSYM)
  {
    lenv_write_global_short(e, k.value.Int, v.type->constructor(v));
  }
}

lenv_t* lenv_add_builtin(lenv_t* e, char* k, lfun_t f)
{
  lval_t fv = lval_fun(f);
  return lenv_write(e, k, fv);
}

void lenv_print(lenv_t* e)
{
  printf("Environment with %zu long variables and %zu short variables\n", e->count, e->short_count);
  printf("LONG:\n");
  for(size_t i = 0; i < e->count; i++)
  {
    printf("#%zu: %s -> ", i, e->symbols[i]);
    lval_println(e->values[i]);
  }
  printf("SHORT:\n");
  for(size_t i = 0; i < e->short_count; i++)
  {
    printf("#%zu: %lu (", i, e->short_symbols[i]);
    char* s = (char*)(e->short_symbols + i);
    for(unsigned j = 0; j < sizeof(long); j++)
    {
      putchar(s[j]);
    }
    printf(") -> (%p, %lu) ", e->short_values[i].type, e->short_values[i].value.Int);
    lval_println(e->short_values[i]);
  }
}

void free_substrings(size_t c, char** e)
{
  for(size_t i = 0; i < c; i++)
  {
    free(e[c]);
  }
  free(e);
}

char* lenv_str(lenv_t* e)
{
  char** prints = malloc(sizeof(char*) * e->count);
  if(!prints)
  {
    return NULL;
  }
  char** sprints = malloc(sizeof(char*) * e->short_count);
  if(!sprints)
  {
    free(prints);
    return NULL;
  }
  size_t length =
    strlen("Environment with long variables and short variables:\n");
  length += 3 * 2 * sizeof(long); // Enough space to print 2 "longs"
  for(size_t i = 0; i < e->count; i++)
  {
    length += strlen("#:  -> ");
    length += strlen(e->symbols[i]);
    length += 3 * sizeof(long);
    prints[i] = lval_str(e->values[i]);
    length += strlen(prints[i]);
  }
  for(size_t i = 0; i < e->short_count; i++)
  {
    length += strlen("#:  -> \n");
    length += sizeof(long); // Maximum length of one short string
    length += 3 * sizeof(long);
    sprints[i] = lval_str(e->short_values[i]);
    length += strlen(sprints[i]);
  }
  length += strlen("LONG:\n") + strlen("SHORT:\n");

  char* result = malloc(length + 1);
  char* ptr = result;
  if(!result)
  {
    free_substrings(e->count, prints);
    free_substrings(e->short_count, sprints);
    return NULL;
  }

  int fail;

  fail = sprintf(
    ptr, "Environment with %zu long variables and %zu short variables:\n",
    e->count, e->short_count
  );
  if(fail <= 0)
  {
    free(result);
    free_substrings(e->count, prints);
    free_substrings(e->short_count, sprints);
    return NULL;
  }
  ptr += fail - 1;

  fail = sprintf(
    ptr, "LONG:\n"
  );
  if(fail <= 0)
  {
    free(result);
    free_substrings(e->count, prints);
    free_substrings(e->short_count, sprints);
    return NULL;
  }
  ptr += fail - 1;

  for(size_t i = 0; i < e->count; i++)
  {
    fail = sprintf(ptr, "#%zu: %s -> %s\n", i, e->symbols[i], prints[i]);
    if(fail <= 0)
    {
      free(result);
      free_substrings(e->count, prints);
      free_substrings(e->short_count, sprints);
      return NULL;
    }
    ptr += fail - 1;
  }

  for(size_t i = 0; i < e->short_count; i++)
  {
    fail = sprintf(ptr, "#%zu: ", i);
    if(fail <= 0)
    {
      free(result);
      free_substrings(e->count, prints);
      free_substrings(e->short_count, sprints);
      return NULL;
    }
    ptr += fail - 1;
    char* sptr = (char*)(&e->short_symbols[i]);
    int j = 0;
    while((sptr[j] != '\0') && (j < sizeof(long)))
    {
      *ptr = sptr[j];
      ptr++;
    }
    fail = sprintf(ptr, "-> %s\n", sprints[i]);
    if(fail <= 0)
    {
      free(result);
      free_substrings(e->count, prints);
      free_substrings(e->short_count, sprints);
      return NULL;
    }
    ptr += fail - 1;
  }

  return result;
}

char* lval_scope_str(lval_t v)
{
  char* result = malloc(
    strlen("Environment with  long variables and  short variables")
     + 2 * 3 * sizeof(long)
     + 1
  );
  int fail = sprintf(
    result,
    "Environment with %zu long variables and %zu short variables",
    v.value.Scope->count,
    v.value.Scope->short_count
  );
  if(fail <= 0)
  {
    free(result);
    return NULL;
  }
  return result;
}

lval_t lval_scope_cons(lval_t v);
lval_t lval_scope_ref_cons(lval_t);

void lval_scope_dest(lval_t v)
{
  lenv_free(v.value.Scope);
}

type_t LVAL_SCOPE = {
  .name = "Scope",
  .constructor = lval_scope_cons,
  .destructor = lval_scope_dest,
  .str = lval_scope_str,
  .free_string = 0
};

type_t LVAL_SCOPE_REF = {
  .name = "Scope (Reference)",
  .constructor = lval_scope_ref_cons,
  .destructor = NULL,
  .str = lval_scope_str,
  .free_string = 0
};

lval_t lval_scope_cons(lval_t v)
{
  if(v.type == &LVAL_SCOPE)
  {
    lval_t r;
    r.type = &LVAL_SCOPE;
    r.value.Scope = lenv_new();
    return r;
  }
  else
  {
    return constructor_error(LVAL_SCOPE, v);
  }
}

lval_t lval_scope_ref_cons(lval_t v)
{
  if((v.type == &LVAL_SCOPE) || (v.type == &LVAL_SCOPE_REF))
  {
    lval_t r = {
      .type = &LVAL_SCOPE_REF,
      .value.Scope = v.value.Scope
    };
    return r;
  }
  else
  {
    return constructor_error(LVAL_SCOPE_REF, v);
  }
}
