cmake_minimum_required(VERSION 2.8.9)
project(Lispy C)

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  set(CMAKE_BUILD_TYPE "Debug")
endif()

if(WIN32)
  set(NO_EDITLINE True)
endif()

include_directories(include)

file(GLOB SOURCES "src/*" "src/ops/*" "src/types/*")

add_executable(lispy ${SOURCES})
if(NO_EDITLINE)
  add_definitions(-DFAKE_READLINE)
else()
  target_link_libraries(lispy editline)
endif()
