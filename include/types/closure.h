#ifndef LAMBDA_INCLUDED
#define LAMBDA_INCLUDED

#include <lval.h>
#include <lenv.h>
#include <lval_lst.h>
#include <types/basic_types.h>
#include <expr.h>

struct closure_t
{
  lenv_t environment;
  lval_t formals;
  lval_t body;
};

extern type_t LVAL_CLOS;

inline void lval_closure_dest(lval_t v)
{
  lenv_free_obj(v.value.Closure->environment);
  lval_free(v.value.Closure->formals);
  lval_free(v.value.Closure->body);
  free(v.value.Closure);
}

char* lval_closure_str(lval_t v);

inline lval_t lval_closure(lenv_t environment, lval_t formals, lval_t body)
{
  lval_t r;
  r.type = &LVAL_CLOS;
  r.value.Closure = malloc(sizeof(closure_t));
  r.value.Closure->environment = environment;
  r.value.Closure->formals = formals;
  r.value.Closure->body = body;
  return r;
}

inline lval_t lval_closure_cons(lval_t v)
{
  lval_t r;
  r.type = &LVAL_CLOS;
  if(v.type == &LVAL_CLOS)
  {
    return lval_closure(
      lenv_copy_obj(v.value.Closure->environment),
      v.value.Closure->formals.type->constructor(v.value.Closure->formals),
      v.value.Closure->body.type->constructor(v.value.Closure->body)
    );
  }
  else
  {
    return constructor_error(LVAL_CLOS, v);
  }
}

#endif
