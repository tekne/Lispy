#ifndef PARSER_INCLUDED
#define PARSER_INCLUDED

#include "lval.h"
#include "expr.h"
#include "lval_lst.h"
#include "types/basic_types.h"

#ifdef _WIN32
#define FAKE_READLINE
#endif

#ifdef FAKE_READLINE

#include <string.h>
#include <ops/file_ops.h>

static inline char* readline(char* prompt)
{
  printf("%s", prompt);
  return read_file_line(stdin);
}

static inline void add_history(char* unused) {}

#else

#include <editline/readline.h>

#endif

lval_t lval_read_expr(char** arr, int is_sexpr);
lval_t lval_read_token(char** arr);
lval_t lval_read_expr(char** arr, int is_sexpr);

inline lval_t lval_read(char* arr)
{
  return lval_read_expr(&arr, -1);
}

#endif
