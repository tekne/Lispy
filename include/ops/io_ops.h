#ifndef IO_OPS_INCLUDED
#define IO_OPS_INCLUDED

#include <lval.h>
#include <lenv.h>

lval_t builtin_print(lenv_t* e, lval_t a);
lval_t builtin_input_str(lenv_t* e, lval_t a);
lval_t builtin_input(lenv_t* e, lval_t a);
lval_t builtin_error(lenv_t* e, lval_t a);

inline lval_t builtin_println(lenv_t* e, lval_t a)
{
  lval_t result = builtin_print(e, a);
  if(result.type != &LVAL_ERR)
  {
    putchar('\n');
  }
  return result;
}

inline lenv_t* lenv_add_io_ops(lenv_t* e)
{
    lenv_add_builtin(e, "print", builtin_print);
    lenv_add_builtin(e, "println", builtin_println);
    lenv_add_builtin(e, "input_str", builtin_input_str);
    lenv_add_builtin(e, "input", builtin_input);
    lenv_add_builtin(e, "error", builtin_error);
}

#endif
