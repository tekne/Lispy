#ifndef EXPR_OPS_INCLUDED
#define EXPR_OPS_INCLUDED

#include "../lval.h"
#include "../lenv.h"
#include "../eval.h"
#include "../expr.h"
#include "../lval_lst.h"

lval_t builtin_head(lenv_t* e, lval_t a);
lval_t builtin_tail(lenv_t* e, lval_t a);
lval_t builtin_list(lenv_t* e, lval_t a);
lval_t builtin_join(lenv_t* e, lval_t a);
lval_t builtin_eval(lenv_t* e, lval_t a);
lval_t builtin_len(lenv_t* e, lval_t a);
lval_t builtin_take(lenv_t* e, lval_t a);
lval_t builtin_place(lenv_t* e, lval_t a);

inline lenv_t* lenv_add_expr_ops(lenv_t* e)
{
  lenv_add_builtin(e, "join", builtin_join);
  lenv_add_builtin(e, "head", builtin_head);
  lenv_add_builtin(e, "tail", builtin_tail);
  lenv_add_builtin(e, "eval", builtin_eval);
  lenv_add_builtin(e, "len", builtin_len);
  lenv_add_builtin(e, "take", builtin_take);
  lenv_add_builtin(e, "place", builtin_place);
  return e;
}


#endif
