#ifndef BASIC_OPS_INCLUDED
#define BASIC_OPS_INCLUDED

#include "../lval.h"
#include "../lenv.h"
#include "../types/basic_types.h"
#include "arithmetic_ops.h"

inline type_t* numeric_type(lval_t* arr, size_t c, int* err, int* hom)
{
  type_t* t = NULL;
  for(int i = 0; i < c; i++)
  {
    if(arr[i].type == &LVAL_REAL)
    {
      if(t != &LVAL_REAL)
      {
        *hom = 0;
      }
      t = &LVAL_REAL;
    }
    else if(arr[i].type == &LVAL_INT)
    {
      if(!t) t = &LVAL_INT;
      if(t != &LVAL_INT) *hom = 0;
    }
    else
    {
      *err = 1;
      return arr[i].type;
    }
  }
  return t;
}

lval_t builtin_add(lenv_t* e, lval_t v);
lval_t builtin_mul(lenv_t* e, lval_t v);
lval_t builtin_sub(lenv_t* e, lval_t v);
lval_t builtin_div(lenv_t* e, lval_t v);
lval_t builtin_lt(lenv_t* e, lval_t a);
lval_t builtin_gt(lenv_t* e, lval_t a);
lval_t builtin_leq(lenv_t* e, lval_t a);
lval_t builtin_geq(lenv_t* e, lval_t a);
lval_t builtin_eq(lenv_t* e, lval_t a);

inline lenv_t* lenv_add_arithmetic_ops(lenv_t* e)
{
  lenv_add_builtin(e, "+", builtin_add);
  lenv_add_builtin(e, "*", builtin_mul);
  lenv_add_builtin(e, "-", builtin_sub);
  lenv_add_builtin(e, "/", builtin_div);
  lenv_add_builtin(e, "<", builtin_lt);
  lenv_add_builtin(e, ">", builtin_gt);
  lenv_add_builtin(e, "<=", builtin_leq);
  lenv_add_builtin(e, ">=", builtin_geq);
  return e;
}


#endif
