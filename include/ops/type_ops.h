#ifndef TYPE_OPS_INCLUDED
#define TYPE_OPS_INCLUDED

#include "../lval.h"
#include "../lenv.h"
#include "../lval_lst.h"
#include "type_ops.h"

lval_t builtin_typ(lenv_t* e, lval_t a);
lval_t builtin_str(lenv_t* e, lval_t a);

inline lenv_t* lenv_add_type_ops(lenv_t* e)
{
  lenv_add_builtin(e, "typ", builtin_typ);
  lenv_add_builtin(e, "str", builtin_str);
  return e;
}

#endif
