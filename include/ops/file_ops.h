#ifndef FILE_OPS_INCLUDED
#define FILE_OPS_INCLUDED

#include <lval.h>
#include <lenv.h>
#include <stdio.h>

char* read_file_line(FILE* f);
lval_t load_file(lenv_t* e, char* str);
lval_t builtin_load(lenv_t* e, lval_t a);
lenv_t* lenv_add_file_ops(lenv_t* e);

#endif
