#ifndef LAMBDA_OPS_INCLUDED
#define LAMBDA_OPS_INCLUDED

#include <lval.h>
#include <lenv.h>

lval_t builtin_lambda(lenv_t* e, lval_t a);
lval_t builtin_var(lenv_t* e, lval_t a, int global);

inline lval_t builtin_def(lenv_t* e, lval_t a)
{
  return builtin_var(e, a, 1);
}
inline lval_t builtin_put(lenv_t* e, lval_t a)
{
  return builtin_var(e, a, 0);
}
inline lenv_t* lenv_add_lambda_ops(lenv_t* e)
{
  lenv_add_builtin(e, "\\", builtin_lambda);
  lenv_add_builtin(e, "def", builtin_def);
  lenv_add_builtin(e, "=", builtin_put);
  return e;
}

#endif
