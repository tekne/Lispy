#ifndef CONTROL_OPS_INCLUDED
#define CONTROL_OPS_INCLUDED

#include <lval.h>
#include <lenv.h>

lval_t builtin_if(lenv_t*, lval_t);
lval_t builtin_switch(lenv_t*, lval_t);

inline lenv_t* lenv_add_control_ops(lenv_t* e)
{
  lenv_add_builtin(e, "if", builtin_if);
  lenv_add_builtin(e, "switch", builtin_switch);
  return e;
}

#endif
