#ifndef LOGICAL_OPS_INCLUDED
#define LOGICAL_OPS_INCLUDED

#include <lval.h>
#include <lenv.h>

inline lval_t to_logic(lval_t v)
{
  if(v.type == &LVAL_REAL)
  {
    return lval_int(v.value.Real != 0);
  }
  else if(v.type == &LVAL_INT)
  {
    return v;
  }
  else
  {
    char* e = lval_str(v);
    lval_t err = expectation_error("LogicType", e);
    free(e);
    return err;
  }
}

lval_t builtin_log_and(lenv_t*, lval_t);
lval_t builtin_log_or(lenv_t*, lval_t);
lval_t builtin_log_xor(lenv_t*, lval_t);
lval_t builtin_log_not(lenv_t*, lval_t);

inline lenv_t* lenv_add_logical_ops(lenv_t* e)
{
  lenv_add_builtin(e, "and", builtin_log_and);
  lenv_add_builtin(e, "or", builtin_log_or);
  lenv_add_builtin(e, "xor", builtin_log_xor);
  lenv_add_builtin(e, "not", builtin_log_not);
  return e;
}

#endif
