#ifndef LVAL_LST_INCLUDED
#define LVAL_LST_INCLUDED

#include "lval.h"
#include "stdio.h"

typedef struct lval_lst {char c;} lval_lst;

inline lval_lst* lval_lst_new()
{
  size_t* n = malloc(sizeof(size_t));
  *n = 0;
  return (lval_lst*)n;
}

inline size_t lval_lst_count(const lval_lst* s)
{
  if(!s) return 0;
  return *(size_t*)s;
}

inline lval_t* lval_lst_arr(lval_lst* s)
{
  if(lval_lst_count(s)) return (lval_t*)(s + sizeof(size_t));
  return NULL;
}

inline lval_lst* lval_lst_resize(lval_lst* s, size_t n)
{
  s = realloc(s, sizeof(size_t) + n * sizeof(lval_t));
  *((size_t*)s) = n;
  return s;
}

inline lval_lst* lval_lst_decrement(lval_lst* s)
{
  if(lval_lst_count(s))
    return lval_lst_resize(s, lval_lst_count(s) - 1);
  return NULL;
}

inline lval_lst* lval_lst_increment(lval_lst* s)
{
  return lval_lst_resize(s, lval_lst_count(s) + 1);
}

inline lval_lst* lval_lst_add(lval_lst* s, lval_t v)
{
  s = lval_lst_increment(s);
  if(!s)
  {
    return NULL;
  }
  size_t c = lval_lst_count(s);
  lval_lst_arr(s)[c - 1] = v;
  return s;
}

inline lval_lst* lval_lst_append(lval_lst* s, size_t n, lval_t* v)
{
  s = lval_lst_resize(s, n + lval_lst_count(s));
  memcpy(lval_lst_count(s) + lval_lst_arr(s), v, n * sizeof(lval_t));
  return s;
}

inline lval_lst* lval_lst_cpy(lval_lst* l)
{
  size_t count = lval_lst_count(l);
  lval_lst* result = malloc(sizeof(lval_t) * count + sizeof(size_t));
  if(result)
  {
    *((size_t*)result) = count;
    lval_t* dest = lval_lst_arr(result);
    lval_t* arr = lval_lst_arr(l);
    for(size_t i = 0; i < count; i++)
    {
      dest[i] = arr[i].type->constructor(arr[i]);
    }
  }
  return result;
}

inline void lval_lst_free(lval_lst* l)
{
  size_t c = lval_lst_count(l);
  lval_t* ptr = lval_lst_arr(l);
  for(int i = 0; i < c; i++)
  {
    lval_free(ptr[i]);
  }
  free(l);
}

void lval_lst_print(lval_lst* s);

char* lval_lst_str(lval_lst* s, char l, char r);

lval_t lval_join(lval_t x, lval_t y);

inline lval_t lval_pop(lval_t v, int i)
{
  lval_t* arr = lval_lst_arr(v.value.List);
  memmove(arr + i, arr + i + 1, sizeof(lval_t) *
    (lval_lst_count(v.value.List) - i));
  v.value.List = lval_lst_decrement(v.value.List);
  return v;
}

inline lval_t lval_get(lval_t v, int i)
{
  return lval_lst_arr(v.value.List)[i];
}

inline lval_t lval_take(lval_t v, int i)
{
  lval_t x = lval_get(v, i);
  v = lval_pop(v, i);
  lval_free(v);
  return x;
}

#endif
