// Simple "environment" containing bindings between variables and names
//TODO: replace with hashtable

#ifndef LENV_INCLUDED
#define LENV_INCLUDED

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

#include "lval.h"
#include "types/basic_types.h"

typedef struct lenv_t
{
  size_t count;
  size_t short_count;
  char** symbols;
  long* short_symbols;
  lval_t* values;
  lval_t* short_values;
  lenv_t* parent;
} lenv_t;

inline lenv_t lenv_new_obj()
{
  lenv_t e;
  e.count = 0;
  e.short_count = 0;
  e.symbols = NULL;
  e.values = NULL;
  e.short_symbols = NULL;
  e.short_values = NULL;
  e.parent = NULL;
  return e;
}

lenv_t lenv_copy_obj(lenv_t e);

inline lenv_t* lenv_new()
{
  lenv_t* e = malloc(sizeof(lenv_t));
  *e = lenv_new_obj();
  return e;
}

inline void lenv_dump_contents(lenv_t* e)
{
  free(e->symbols);
  free(e->values);
  e->symbols = NULL;
  e->short_symbols = NULL;
  e->values = NULL;
  e->short_values = NULL;
  e->count = 0;
  e->short_count = 0;
}

inline void lenv_free_obj(lenv_t e)
{
  for(int i = 0; i < e.count; i++)
  {
    free(e.symbols[i]);
    lval_free(e.values[i]);
  }
  for(int i = 0; i < e.short_count; i++)
  {
    lval_free(e.short_values[i]);
  }
  free(e.symbols);
  free(e.short_symbols);
  free(e.values);
  free(e.short_values);
}

inline void lenv_free(lenv_t* e)
{
  lenv_free_obj(*e);
  free(e);
}

lval_t* lenv_get_short_ptr(lenv_t* e, long k);
lval_t* lenv_get_long_ptr(lenv_t* e, char* k);

inline lval_t* lenv_get_ptr(lenv_t* e, char* k)
{
  long lk = str_to_sstr(k);
  if(lk)
  {
    return lenv_get_short_ptr(e, lk);
  }
  else
  {
    return lenv_get_long_ptr(e, k);
  }
}

inline lval_t lenv_get(lenv_t* e, char* k)
{
  lval_t* ptr = lenv_get_ptr(e, k);
  if(ptr)
  {
    return *ptr;
  }
  else
  {
    char* err = malloc(strlen(k) + strlen("Unbound symbol ") + 1);
    sprintf(err, "Unbound symbol %s", k);
    lval_t result = lval_err(err);
    free(err);
    return result;
  }
}

inline lval_t lenv_get_sym(lenv_t* e, lval_t v)
{
  if(v.type == &LVAL_SYM)
  {
    lval_t* ptr = lenv_get_ptr(e, v.value.Str);
    if(ptr) return *ptr;
  }
  else if(v.type == &LVAL_SSYM)
  {
    lval_t* ptr = lenv_get_short_ptr(e, v.value.Int);
    if(ptr) return *ptr;
  }
  return v;
}

ptrdiff_t lenv_long_pos(lenv_t* e, char* k, int* eq);
ptrdiff_t lenv_short_pos(lenv_t* e, long k, int* eq);

void lenv_print(lenv_t* e);

lenv_t* lenv_insert_long(lenv_t* e, size_t pos, char* k, lval_t v);
lenv_t* lenv_insert_short(lenv_t* e, size_t pos, long k, lval_t v);

inline lenv_t* lenv_write_long(lenv_t* e, char* k, lval_t v)
{
  int eq = 1;
  ptrdiff_t pos = lenv_long_pos(e, k, &eq);
  if(eq)
  {
    lval_free(e->values[pos]);
    e->values[pos] = v;
  }
  else
  {
    lenv_insert_long(e, pos, k, v);
  }
  return e;
}

inline lenv_t* lenv_write_short(lenv_t* e, long k, lval_t v)
{
  int eq = 1;
  ptrdiff_t pos = lenv_short_pos(e, k, &eq);
  if(eq)
  {
    lval_free(e->short_values[pos]);
    e->short_values[pos] = v;
  }
  else
  {
    lenv_insert_short(e, pos, k, v);
  }
  return e;
}

inline lenv_t* lenv_write(lenv_t* e, char* k, lval_t v)
{
  long lk = str_to_sstr(k);
  if(lk)
  {
    return lenv_write_short(e, lk, v);
  }
  else
  {
    return lenv_write_long(e, k, v);
  }
}

inline lenv_t* lenv_put_long(lenv_t* e, char* k, lval_t v)
{
  return lenv_write_long(e, k, v.type->constructor(v));
}
inline lenv_t* lenv_put_short(lenv_t* e, long k, lval_t v)
{
  return lenv_write_short(e, k, v.type->constructor(v));
}
inline lenv_t* lenv_put(lenv_t* e, char* k, lval_t v)
{
  return lenv_write(e, k, v.type->constructor(v));
}

inline lenv_t* lenv_put_sym(lenv_t* e, lval_t k, lval_t v)
{
  lval_t n = v.type->constructor(v);
  if(k.type == &LVAL_SYM)
  {
    return lenv_write(e, k.value.Str, n);
  }
  else if(k.type == &LVAL_SSYM)
  {
    return lenv_write_short(e, k.value.Int, n);
  }
  return e;
}

inline void lenv_write_global_short(lenv_t* e, long k, lval_t v)
{
  if(e)
  {
    if(e->parent)
    {
      lenv_write_global_short(e->parent, k, v);
    }
    else
    {
      lenv_write_short(e, k, v);
    }
  }
}

inline void lenv_write_global_long(lenv_t* e, char* k, lval_t v)
{
  if(e)
  {
    if(e->parent)
    {
      lenv_write_global_long(e->parent, k, v);
    }
    else
    {
      lenv_write_long(e, k, v);
    }
  }
}

inline void lenv_write_global(lenv_t* e, char* k, lval_t v)
{
  if(e)
  {
    if(e->parent)
    {
      lenv_write_global(e->parent, k, v);
    }
    else
    {
      lenv_write(e, k, v);
    }
  }
}

inline void lenv_put_global(lenv_t* e, char* k, lval_t v)
{
  lenv_write_global(e, k, v.type->constructor(v));
}

inline void lenv_put_sym_global(lenv_t* e, lval_t k, lval_t v)
{
  if(k.type == &LVAL_SYM)
  {
    lenv_put_global(e, k.value.Str, v);
  }
  else if(k.type == &LVAL_SSYM)
  {
    lenv_write_global_short(e, k.value.Int, v.type->constructor(v));
  }
}

inline lenv_t* lenv_add_builtin(lenv_t* e, char* k, lfun_t f)
{
  lval_t fv = lval_fun(f);
  return lenv_write(e, k, fv);
}

void lenv_print(lenv_t* e);

lval_t lval_scope_cons(lval_t v);

inline void lval_scope_dest(lval_t v)
{
  lenv_free(v.value.Scope);
}

extern type_t LVAL_SCOPE;
extern type_t LVAL_SCOPE_REF;

lval_t lval_scope_cons(lval_t v);
lval_t lval_scope_ref_cons(lval_t v);

char* lenv_str(lenv_t* e);
inline char* lval_scope_str(lval_t v)
{
  return lenv_str(v.value.Scope);
}

#endif
