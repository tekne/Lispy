#ifndef LVAL_INCLUDED
#define LVAL_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct lval_t;
typedef struct lval_t lval_t;
struct lenv_t;
typedef struct lenv_t lenv_t;
struct lval_lst;
typedef struct lval_lst lval_lst;
struct closure_t;
typedef struct closure_t closure_t;

typedef lval_t(*lfun_t)(lenv_t*, lval_t);
typedef lval_t(*lcons_t)(lval_t);
typedef void(*ldest_t)(lval_t);
typedef char*(*lstr_t)(lval_t);

typedef struct type_t
{
  char* name;
  lcons_t constructor;
  ldest_t destructor;
  lstr_t str;
  unsigned int free_string : 1;
} type_t;

extern type_t LVAL_TYPE;
extern type_t LVAL_ERR;

inline type_t type_new(char* name,
                       lcons_t constructor,
                       ldest_t destructor,
                       lstr_t str
                    )
{
  type_t t;
  t.name = malloc(strlen(name));
  strcpy(t.name, name);
  t.constructor = constructor;
  t.destructor = destructor;
  t.str = str;
  t.free_string = 1;
  return t;
}

inline type_t type_copy(type_t t)
{
  type_t n = t;
  if(t.free_string)
  {
    n.name = malloc(strlen(t.name));
    strcpy(n.name, t.name);
  }
  return n;
}

inline void type_free(type_t t)
{
  if(t.free_string)
  {
    free(t.name);
  }
}

typedef union
{
  char* Str;
  double Real;
  long Int;
  lval_lst* List;
  void* Void;
  lfun_t Function;
  type_t* Type;
  lenv_t* Scope;
  closure_t* Closure;
  FILE* File;
  char ShortStr[sizeof(long)];
} untagged_lval_t;

struct lval_t {
  type_t* type;
  untagged_lval_t value;
};

inline void free_destructor(lval_t v) {free(v.value.Void);}

inline char* identity_str(lval_t v)
{
  char* result = malloc(strlen(v.value.Str));
  strcpy(result, v.value.Str);
  return result;
}

inline lval_t lval_err(const char* x)
{
  lval_t v;
  v.type = &LVAL_ERR;
  v.value.Str = malloc(strlen(x) + 1);
  strcpy(v.value.Str, x);
  return v;
}

lval_t constructor_error(type_t t, lval_t v);
lval_t expectation_error(char* expected, char* got);
lval_t expectation_error_type(char* expected, char* got, char* type);

inline lval_t lval_err_cons(lval_t v)
{
  if(v.type == &LVAL_ERR)
  {
    lval_t n;
    n.type = &LVAL_ERR;
    n.value.Str = malloc(strlen(v.value.Str));
    strcpy(n.value.Str, v.value.Str);
    return n;
  }
  else
  {
    return constructor_error(LVAL_ERR, v);
  }
}

inline char* lval_err_str(lval_t v)
{
  char* result = malloc(1 + strlen("ERROR: ") + strlen(v.value.Str));
  sprintf(result, "ERROR: %s", v.value.Str);
  return result;
}

inline char* lval_typ_str(lval_t v)
{
    char* string = v.value.Type->name;
    char* result = malloc(1 + strlen(string));
    strcpy(result, string);
    return result;
}

inline void lval_typ_dest(lval_t v)
{
  type_free(*v.value.Type);
  free(v.value.Type);
}

inline lval_t lval_typ(char* n, lcons_t c, ldest_t d, lstr_t s)
{
  type_t* t = malloc(sizeof(type_t));
  *t = type_new(n, c, d, s);
  lval_t v;
  v.type = &LVAL_TYPE;
  v.value.Type = t;
  return v;
}

inline lval_t lval_typ_obj(type_t* t)
{
  type_t* n = NULL;
  if(t)
  {
    n = malloc(sizeof(type_t));
    *n = type_copy(*t);
  }
  lval_t v = {
    .type = &LVAL_TYPE,
    .value.Type = n
  };
  return v;
}

inline lval_t lval_typ_cons(lval_t v)
{
  return lval_typ_obj(v.value.Type);
}

inline char* lval_str(const lval_t v)
{
  if((!v.type)||(!v.type->str))
  {
    char* name;
    if(v.type && v.type->name)
    {
      name = v.type->name;
    }
    else
    {
      name = "NULL TYPE";
    }
    char* result = malloc(1
      + strlen("< @ >") + strlen(name) + 8*sizeof(char*) + strlen("0x")
    );
    sprintf(result, "<%s @ %p>", name, &v);
    return result;
  }
  return v.type->str(v);
}

inline void lval_print(const lval_t v)
{
  if(!v.type->str)
  {
    printf("<%s @ %p>", v.type->name, &v);
    return;
  }
  char* result = v.type->str(v);
  printf("%s", result);
  free(result);
}

inline void lval_println(const lval_t v)
{
  lval_print(v);
  putchar('\n');
}

inline void lval_free(lval_t v)
{
  if(!v.type) return;
  if(v.type->destructor)
  {
    v.type->destructor(v);
  }
}

inline lval_t lval_copy(const lval_t v)
{
  if(v.type->constructor)
  {
    return v.type->constructor(v);
  }
  else
  {
    return v;
  }
}

#endif
