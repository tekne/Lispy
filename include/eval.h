#ifndef EVAL_INCLUDED
#define EVAL_INCLUDED

#include <lval.h>
#include <expr.h>
#include <lval_lst.h>
#include <types/closure.h>

lval_t lval_eval_sexpr(lenv_t* e, lval_t v);

inline lval_t lval_eval(lenv_t* e, lval_t v)
{
  if((v.type == &LVAL_SYM) || (v.type == &LVAL_SSYM))
  {
    lval_t x = lenv_get_sym(e, v);
    return x.type->constructor(x);
  }
  if(v.type == &LVAL_SEXPR) {return lval_eval_sexpr(e, v);}
  return v;
}

#endif
