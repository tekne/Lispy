An interpreter for a simplified version of the Lisp language, inspired by Build Your Own Lisp.

Compile with ``mkdir  build && cd build && cmake .. && make``. Has no dependencies other than a C compiler.
